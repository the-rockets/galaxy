package com.manic.galaxy.infrastructure.ktor

import com.manic.galaxy.infrastructure.event.EventService
import com.manic.galaxy.infrastructure.ktor.galaxies.configureGalaxies
import com.manic.galaxy.infrastructure.ktor.items.configureItems
import com.manic.galaxy.infrastructure.ktor.pilots.configurePilots
import com.manic.galaxy.infrastructure.ktor.spacestations.configureSpaceStations
import com.manic.galaxy.infrastructure.ktor.systems.configureSystems
import com.manic.galaxy.infrastructure.ktor.users.configureUsers
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.util.*
import io.ktor.websocket.*
import org.slf4j.LoggerFactory

fun Application.configureRouting(environment: Environment, eventService: EventService) {
    val logger = LoggerFactory.getLogger(this::class.java)
    install(AutoHeadResponse)
    install(WebSockets)
    install(StatusPages) {
        exception<AuthenticationException> { cause ->
            call.respond(HttpStatusCode.Unauthorized)
        }
        exception<AuthorizationException> { cause ->
            call.respond(HttpStatusCode.Forbidden)
        }
        exception<IllegalArgumentException> { cause ->
            logger.info(cause.message)
            call.respond(HttpStatusCode.BadRequest, BadRequestDto(cause.message))
        }
        exception<DataConversionException> { cause ->
            logger.info(cause.message)
            call.respond(HttpStatusCode.BadRequest, BadRequestDto(cause.message))
        }
        exception<MissingRequestParameterException> { cause ->
            logger.info(cause.message)
            call.respond(HttpStatusCode.BadRequest, BadRequestDto(cause.message))
        }
    }


    routing {
        configureWebSockets(environment.jwtAccessTokenSigner, eventService)
        configureUsers(environment)
        configureGalaxies(environment)
        configureSystems(environment)
        configurePilots(environment)
        configureSpaceStations(environment)
        configureItems(environment)
    }
}

class AuthenticationException : RuntimeException()
class AuthorizationException : RuntimeException()
data class BadRequestDto(val message: String?, val code: Int = 400)
