package com.manic.galaxy.infrastructure.ktor

import com.manic.galaxy.application.PilotService
import com.manic.galaxy.infrastructure.mongodb.MongoPilotRepository
import io.ktor.application.*
import kotlinx.coroutines.*

fun Application.configureScheduling(environment: Environment) = launch {
    createPilotScheduler(environment.pilotService, environment.pilotRepository as MongoPilotRepository)
}

suspend fun createPilotScheduler(
    pilotService: PilotService,
    mongoPilotRepository: MongoPilotRepository
) = withContext(Dispatchers.IO) {
    while (isActive) {
        delay(1_000)
        val pilots = mongoPilotRepository.findArrived()
        pilots.map { it.id }.forEach { pilotService.reachDestinations(it) }
    }
}
