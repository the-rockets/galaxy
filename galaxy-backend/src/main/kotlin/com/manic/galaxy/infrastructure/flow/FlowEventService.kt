package com.manic.galaxy.infrastructure.flow

import com.manic.galaxy.infrastructure.event.EventService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import kotlin.reflect.KClass

class FlowEventService(private val coroutineScope: CoroutineScope) : EventService {
    private val topics = mutableMapOf<KClass<out Any>, MutableSharedFlow<Any>>()

    override suspend fun publish(events: List<Any>) {
        events.forEach {
            val flow = topics[it::class]
            logger.debug("Emitting event ${it::class.simpleName}.")
            flow?.emit(it)
        }
    }

    override fun <T : Any> subscribe(event: KClass<T>, callback: suspend (T) -> Unit) {
        val flow = topics.getOrPut(event, ::MutableSharedFlow)
        coroutineScope.launch {
            flow.map { it as T }.collect(callback)
        }
    }

    companion object {
        private val logger = LoggerFactory.getLogger(this::class.java)
    }
}
