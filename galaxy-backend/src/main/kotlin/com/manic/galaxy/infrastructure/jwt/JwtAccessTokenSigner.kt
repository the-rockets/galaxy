package com.manic.galaxy.infrastructure.jwt

import com.auth0.jwt.JWT
import com.auth0.jwt.JWTVerifier
import com.auth0.jwt.algorithms.Algorithm
import com.manic.galaxy.domain.refreshtoken.AccessToken
import com.manic.galaxy.domain.refreshtoken.AccessTokenSigner
import com.manic.galaxy.domain.refreshtoken.SignedAccessToken
import com.manic.galaxy.infrastructure.ktor.AuthenticationException
import java.lang.Exception
import java.util.*

class JwtAccessTokenSigner: AccessTokenSigner {
    private val algorithm = Algorithm.HMAC256("secret")
    val jwtVerifier: JWTVerifier = JWT.require(algorithm).build()

    override fun sign(accessToken: AccessToken): SignedAccessToken {
        val value = JWT.create()
            .withSubject(accessToken.userId.toString())
            .withExpiresAt(Date.from(accessToken.expiresAt))
            .sign(algorithm)

        return SignedAccessToken(value)
    }

    override fun requireValid(signedAccessToken: SignedAccessToken) {
        try {
            jwtVerifier.verify(signedAccessToken.value)
        } catch (exception: Exception) {
            throw AuthenticationException()
        }
    }
}
