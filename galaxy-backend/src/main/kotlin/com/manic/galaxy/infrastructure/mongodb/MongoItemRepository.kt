package com.manic.galaxy.infrastructure.mongodb

import com.manic.galaxy.domain.item.Item
import com.manic.galaxy.domain.item.ItemRepository
import com.manic.galaxy.domain.item.ItemType
import com.manic.galaxy.domain.shared.Slice
import com.manic.galaxy.infrastructure.event.EventService
import com.mongodb.client.model.Filters
import com.mongodb.client.model.Indexes
import com.mongodb.reactivestreams.client.MongoCollection
import com.mongodb.reactivestreams.client.MongoDatabase
import java.util.*

class MongoItemRepository(database: MongoDatabase, eventService: EventService) : MongoRepository(eventService),
    ItemRepository {
    private val collection: MongoCollection<Item> = getCollection(database, Item::class)

    init {
        collection.createIndex(Indexes.ascending("galaxyId"))
        collection.createIndex(Indexes.ascending("type"))
    }

    override suspend fun save(items: Set<Item>): Set<Item> {
        save(collection, items)
        return items
    }

    override suspend fun get(itemId: UUID): Item {
        return requireNotNull(find(collection, Filters.eq("_id", itemId))) { "item.unknownId" }
    }

    override suspend fun list(galaxyId: UUID, slice: Slice): List<Item> {
        return find(collection, slice, Filters.eq("galaxyId", galaxyId)).toList()
    }

    override suspend fun find(itemType: ItemType): List<Item> {
        return findAll(collection, Filters.eq("type", itemType.name)).toList()
    }
}
