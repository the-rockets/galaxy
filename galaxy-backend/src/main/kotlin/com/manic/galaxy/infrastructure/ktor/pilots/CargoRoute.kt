package com.manic.galaxy.infrastructure.ktor.pilots

import com.manic.galaxy.application.CargoService
import com.manic.galaxy.domain.shared.Slice
import com.manic.galaxy.infrastructure.ktor.AccessTokenPrincipal
import com.manic.galaxy.infrastructure.ktor.toUUID
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.util.*
import java.util.*

fun Route.configureCargo(cargoService: CargoService) = authenticate {
    get("/api/pilots/{id}/cargo") {
        val principal: AccessTokenPrincipal = call.principal()!!
        val id: UUID = call.parameters.getOrFail("id").toUUID()
        val limit: Int? = call.request.queryParameters["limit"]?.toInt()
        val offset: Int? = call.request.queryParameters["offset"]?.toInt()

        call.respond(cargoService.listCargo(principal.userId, id, Slice(limit, offset)))
    }
}
