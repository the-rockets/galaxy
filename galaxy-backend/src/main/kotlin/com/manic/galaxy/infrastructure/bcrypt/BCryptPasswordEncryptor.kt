package com.manic.galaxy.infrastructure.bcrypt

import at.favre.lib.crypto.bcrypt.BCrypt
import com.manic.galaxy.domain.user.Password
import com.manic.galaxy.domain.user.PasswordEncryptor

class BCryptPasswordEncryptor : PasswordEncryptor {
    override fun encrypt(rawPassword: String): Password {
        return Password(BCrypt.withDefaults().hashToString(10, rawPassword.toCharArray()))
    }

    override fun requireMatch(password: Password, rawPassword: String) {
        val result = BCrypt.verifyer().verify(rawPassword.toCharArray(), password.value)
        require(result.verified) { "user.invalidPassword" }
    }
}
