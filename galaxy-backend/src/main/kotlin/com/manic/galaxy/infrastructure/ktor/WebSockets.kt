package com.manic.galaxy.infrastructure.ktor

import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.manic.galaxy.domain.pilot.DestinationReached
import com.manic.galaxy.infrastructure.event.EventService
import com.manic.galaxy.infrastructure.jackson.createKtorModule
import com.manic.galaxy.infrastructure.jwt.JwtAccessTokenSigner
import io.ktor.http.cio.websocket.*
import io.ktor.routing.*
import io.ktor.util.*
import io.ktor.websocket.*
import org.slf4j.LoggerFactory
import java.util.*

private val logger = LoggerFactory.getLogger("websocket")

fun Route.configureWebSockets(jwtAccessTokenSigner: JwtAccessTokenSigner, eventService: EventService) {
    val objectMapper = jacksonObjectMapper().apply {
        registerModule(JavaTimeModule())
        registerModule(createKtorModule())
        configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
    }

    val connections = Collections.synchronizedSet<Connection>(LinkedHashSet())

    eventService.subscribe(DestinationReached::class) { event ->
        connections.filter { it.userId == event.userId }.forEach {
            val message = Message(event::class.simpleName!!, event)
            it.session.send(objectMapper.writeValueAsString(message))
        }
    }

    webSocket("/events") {
        val accessToken = call.request.queryParameters.getOrFail<String>("access-token")
        val credential = jwtAccessTokenSigner.jwtVerifier.verify(accessToken)
        val principal = AccessTokenPrincipal(UUID.fromString(credential.subject))
        val connection = Connection(this, principal.userId)
        logger.info("Adding connection '${connection.userId}'")
        connections += connection
        try {
            for (frame in incoming) {
                continue
            }
        } catch (e: Exception) {
            logger.error("Websocket execution failed.",e)
        } finally {
            logger.info("Removing connection '${connection.userId}'")
            connections -= connection
        }

    }
}

data class Message(
    val event: String,
    val payload: Any
)

class Connection(val session: DefaultWebSocketServerSession, val userId: UUID) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Connection

        if (userId != other.userId) return false

        return true
    }

    override fun hashCode(): Int {
        return userId.hashCode()
    }
}
