package com.manic.galaxy.infrastructure.ktor.users

import com.manic.galaxy.infrastructure.ktor.Environment
import io.ktor.routing.*

fun Route.configureUsers(environment: Environment) {
    configureUsers(environment.userService)
}
