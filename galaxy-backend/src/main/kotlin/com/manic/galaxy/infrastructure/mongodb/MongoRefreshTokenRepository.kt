package com.manic.galaxy.infrastructure.mongodb

import com.manic.galaxy.domain.refreshtoken.RefreshToken
import com.manic.galaxy.domain.refreshtoken.RefreshTokenRepository
import com.manic.galaxy.infrastructure.event.EventService
import com.mongodb.client.model.Filters
import com.mongodb.client.model.IndexOptions
import com.mongodb.client.model.Indexes
import com.mongodb.reactivestreams.client.MongoDatabase
import java.util.*
import java.util.concurrent.TimeUnit

class MongoRefreshTokenRepository(database: MongoDatabase, eventService: EventService) : MongoRepository(eventService), RefreshTokenRepository {
    private val collection = getCollection(database, RefreshToken::class)

    init {
        collection.createIndex(
            Indexes.ascending("expiresAt"),
            IndexOptions().expireAfter(0, TimeUnit.SECONDS)
        )
    }

    override suspend fun save(refreshToken: RefreshToken): RefreshToken {
        return save(collection, refreshToken)
    }

    override suspend fun get(refreshTokenId: UUID): RefreshToken {
        val aggregate = find(collection, Filters.eq("_id", refreshTokenId))
        return requireNotNull(aggregate) { "refreshToken.unknownId" }
    }

    override suspend fun delete(refreshToken: RefreshToken) {
        delete(collection, refreshToken)
    }
}
