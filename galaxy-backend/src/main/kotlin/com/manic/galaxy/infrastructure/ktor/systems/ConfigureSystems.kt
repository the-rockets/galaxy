package com.manic.galaxy.infrastructure.ktor.systems

import com.manic.galaxy.infrastructure.ktor.Environment
import io.ktor.routing.*

fun Route.configureSystems(environment: Environment) {
    configureSystemRoutes(environment.systemRouteService)
    configureSpaceStations(environment.spaceStationService)
}
