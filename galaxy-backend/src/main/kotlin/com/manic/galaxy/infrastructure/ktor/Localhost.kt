package com.manic.galaxy.infrastructure.ktor

import com.manic.galaxy.domain.user.Email
import io.ktor.application.*
import kotlinx.coroutines.launch

fun Application.configureLocalhost(environment: Environment) {
    launch(coroutineContext) {
        runCatching {
            environment.userService.createUser(
                Email("admin@galaxy.com"),
                "admin"
            )
        }
        runCatching {
            environment.galaxyService.createGalaxy("Cepheus")
        }
    }
}
