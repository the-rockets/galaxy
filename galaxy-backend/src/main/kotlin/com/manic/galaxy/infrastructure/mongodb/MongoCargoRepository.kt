package com.manic.galaxy.infrastructure.mongodb

import com.manic.galaxy.domain.cargo.Cargo
import com.manic.galaxy.domain.cargo.CargoRepository
import com.manic.galaxy.domain.shared.Slice
import com.manic.galaxy.infrastructure.event.EventService
import com.mongodb.client.model.Filters
import com.mongodb.client.model.Indexes
import com.mongodb.reactivestreams.client.MongoCollection
import com.mongodb.reactivestreams.client.MongoDatabase
import java.util.*

class MongoCargoRepository(database: MongoDatabase, eventService: EventService) : MongoRepository(eventService),
    CargoRepository {
    private val collection: MongoCollection<Cargo> = getCollection(database, Cargo::class)

    init {
        collection.createIndex(
            Indexes.compoundIndex(
                Indexes.ascending("pilotId"),
                Indexes.ascending("itemId"),
            )
        )
    }

    override suspend fun save(cargo: Cargo): Cargo {
        return save(collection, cargo)
    }

    override suspend fun find(pilotId: UUID, itemId: UUID): Cargo? {
        return find(
            collection,
            Filters.and(
                Filters.eq("pilotId", pilotId),
                Filters.eq("itemId", itemId),
            )
        )
    }

    override suspend fun get(pilotId: UUID, itemId: UUID): Cargo {
        return requireNotNull(find(pilotId, itemId)) { "cargo.unknownId" }
    }

    override suspend fun delete(cargo: Cargo) {
        delete(collection, cargo)
    }

    override suspend fun list(pilotId: UUID, slice: Slice): List<Cargo> {
        return find(collection, slice, Filters.eq("pilotId", pilotId)).toList()
    }
}
