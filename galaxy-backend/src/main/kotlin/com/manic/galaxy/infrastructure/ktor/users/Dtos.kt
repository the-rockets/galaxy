package com.manic.galaxy.infrastructure.ktor.users

import com.manic.galaxy.domain.user.Email
import com.manic.galaxy.domain.user.User
import java.util.*

sealed class PostSignIn {
    data class Body(
        val email: Email,
        val password: String
    )

    data class Response(
        val refreshToken: UUID,
        val accessToken: String
    )
}

sealed class PostAccess {
    data class Body(
        val refreshToken: UUID
    )

    data class Response(
        val accessToken: String
    )
}

sealed class PostSignOut {
    data class Body(
        val refreshToken: UUID
    )
}

sealed class User {
    data class Response(
        val id: UUID,
        val email: Email,
        val selectedPilotId: UUID?
    ) {
        companion object {
            fun fromUser(user: User): Response {
                return Response(user.id, user.email, user.selectedPilotId)
            }
        }
    }
}

sealed class PostSelectPilot {
    data class Body(
        val pilotId: UUID
    )
}
