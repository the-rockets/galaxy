package com.manic.galaxy.infrastructure.ktor.users

import com.manic.galaxy.application.UserService
import com.manic.galaxy.infrastructure.ktor.AccessTokenPrincipal
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*

fun Route.configureUsers(userService: UserService) {
    post("/api/users/sign-in") {
        val body: PostSignIn.Body = call.receive()
        val refreshToken = userService.signIn(body.email, body.password)
        val signedAccessToken = userService.access(refreshToken.id)
        call.respond(PostSignIn.Response(refreshToken.id, signedAccessToken.value))
    }

    post("/api/users/access") {
        val body: PostAccess.Body = call.receive()
        val signedAccessToken = userService.access(body.refreshToken)
        call.respond(PostAccess.Response(signedAccessToken.value))
    }

    post("/api/users/sign-out") {
        val body: PostSignOut.Body = call.receive()
        userService.signOut(body.refreshToken)
        call.respond(HttpStatusCode.OK)
    }

    authenticate {
        get ("/api/users/self") {
            val principal: AccessTokenPrincipal = call.principal()!!
            val user = userService.getUser(principal.userId)
            call.respond(User.Response.fromUser(user))
        }

        post("/api/users/self/select-pilot") {
            val principal: AccessTokenPrincipal = call.principal()!!
            val body = call.receive<PostSelectPilot.Body>()
            val user = userService.selectPilot(principal.userId, body.pilotId)
            call.respond(User.Response.fromUser(user))
        }
    }
}
