package com.manic.galaxy.infrastructure.event

import com.manic.galaxy.application.GoodsService
import com.manic.galaxy.domain.spacestation.SpaceStationCreated
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class GoodsSubscriber(
    eventService: EventService,
    private val goodsService: GoodsService,
) {
    init {
        eventService.subscribe(SpaceStationCreated::class, this::onSpaceStationCreated)
    }

    private suspend fun onSpaceStationCreated(spaceStationCreated: SpaceStationCreated) = withContext(Dispatchers.IO) {
        goodsService.createGoods(spaceStationCreated.id)
    }
}
