package com.manic.galaxy.infrastructure.mongodb

import com.manic.galaxy.domain.system.SystemName
import com.manic.galaxy.domain.user.Email
import com.manic.galaxy.domain.user.Password
import org.bson.BsonReader
import org.bson.BsonWriter
import org.bson.codecs.Codec
import org.bson.codecs.DecoderContext
import org.bson.codecs.EncoderContext
import org.bson.codecs.configuration.CodecRegistries
import org.bson.codecs.configuration.CodecRegistry
import java.time.Duration

fun createCodecs(): CodecRegistry = CodecRegistries.fromCodecs(
    EmailCodec(),
    PasswordCodec(),
    SystemNameCodec(),
    DurationCodec()
)

class EmailCodec : Codec<Email> {
    override fun encode(writer: BsonWriter, value: Email, context: EncoderContext) {
        writer.writeString(value.value)
    }

    override fun getEncoderClass(): Class<Email> {
        return Email::class.java
    }

    override fun decode(reader: BsonReader, context: DecoderContext): Email {
        return Email(reader.readString())
    }
}

class PasswordCodec : Codec<Password> {
    override fun encode(writer: BsonWriter, value: Password, context: EncoderContext) {
        writer.writeString(value.value)
    }

    override fun getEncoderClass(): Class<Password> {
        return Password::class.java
    }

    override fun decode(reader: BsonReader, context: DecoderContext): Password {
        return Password(reader.readString())
    }
}

class SystemNameCodec : Codec<SystemName> {
    override fun encode(writer: BsonWriter, value: SystemName, context: EncoderContext) {
        writer.writeString(value.value)
    }

    override fun getEncoderClass(): Class<SystemName> {
        return SystemName::class.java
    }

    override fun decode(reader: BsonReader, context: DecoderContext): SystemName {
        return SystemName(reader.readString())
    }
}

class DurationCodec : Codec<Duration> {
    override fun encode(writer: BsonWriter, value: Duration, context: EncoderContext) {
        writer.writeInt64(value.seconds)
    }

    override fun getEncoderClass(): Class<Duration> {
        return Duration::class.java
    }

    override fun decode(reader: BsonReader, context: DecoderContext): Duration {
        return Duration.ofSeconds(reader.readInt64())
    }
}
