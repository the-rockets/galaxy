package com.manic.galaxy.infrastructure.ktor.pilots

import com.manic.galaxy.application.PilotService
import com.manic.galaxy.application.TradingService
import com.manic.galaxy.domain.shared.Slice
import com.manic.galaxy.infrastructure.ktor.AccessTokenPrincipal
import com.manic.galaxy.infrastructure.ktor.toUUID
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.util.*
import java.util.*

fun Route.configurePilots(pilotService: PilotService, tradingService: TradingService) = authenticate {
    post("/api/pilots") {
        val principal: AccessTokenPrincipal = call.principal()!!
        val body: PostPilots.Body = call.receive()

        call.respond(pilotService.createPilot(principal.userId, body.galaxyId, body.name))
    }

    get("/api/pilots") {
        val principal: AccessTokenPrincipal = call.principal()!!
        val limit: Int? = call.request.queryParameters["limit"]?.toInt()
        val offset: Int? = call.request.queryParameters["offset"]?.toInt()

        call.respond(pilotService.listPilots(principal.userId, Slice(limit, offset)))
    }

    post("/api/pilots/{id}/fly-to") {
        val principal: AccessTokenPrincipal = call.principal()!!
        val id: UUID = call.parameters.getOrFail("id").toUUID()
        val body = call.receive<PostFlyTo.Body>()

        call.respond(pilotService.flyTo(principal.userId, id, body.systemId))
    }

    post("/api/pilots/{id}/dock-at") {
        val principal: AccessTokenPrincipal = call.principal()!!
        val id: UUID = call.parameters.getOrFail("id").toUUID()
        val body = call.receive<PostDockAt.Body>()
        call.respond(pilotService.dockAt(principal.userId, id, body.spaceStationId))
    }

    post("/api/pilots/{id}/buy-item") {
        val principal: AccessTokenPrincipal = call.principal()!!
        val id: UUID = call.parameters.getOrFail("id").toUUID()
        val body = call.receive<PostBuyItem.Body>()
        call.respond(tradingService.buyItem(principal.userId, id, body.itemId, body.amount))
    }

    post("/api/pilots/{id}/sell-item") {
        val principal: AccessTokenPrincipal = call.principal()!!
        val id: UUID = call.parameters.getOrFail("id").toUUID()
        val body = call.receive<PostSellItem.Body>()
        call.respond(tradingService.sellItem(principal.userId, id, body.itemId, body.amount))
    }
}
