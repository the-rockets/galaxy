package com.manic.galaxy.infrastructure.ktor

import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*

fun Application.configureHTTP() {
    install(Compression)
    install(CORS) {
        method(HttpMethod.Options)
        method(HttpMethod.Post)
        method(HttpMethod.Put)
        method(HttpMethod.Delete)
        method(HttpMethod.Get)
        header(HttpHeaders.Authorization)
        allowNonSimpleContentTypes = true
        hosts.add("http://localhost:3000")
    }
    install(DefaultHeaders)
}
