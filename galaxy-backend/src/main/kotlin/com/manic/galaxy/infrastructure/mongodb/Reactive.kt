package com.manic.galaxy.infrastructure.mongodb

import com.mongodb.reactivestreams.client.FindPublisher
import org.reactivestreams.Publisher
import org.reactivestreams.Subscriber
import org.reactivestreams.Subscription
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

suspend fun <T> Publisher<T>.toList(): List<T> = suspendCoroutine {
    this.subscribe(ListSubscriber(it))
}

suspend fun <T> FindPublisher<T>.toFirst(): T? = suspendCoroutine {
    this.first().subscribe(SingleSubscriber(it))
}

suspend fun Publisher<Long>.toFirst(): Long = suspendCoroutine {
    this.subscribe(SingleSubscriber(it))
} ?: 0L

suspend fun <T> Publisher<T>.toUnit(): Unit = suspendCoroutine {
    this.subscribe(UnitSubscriber(it))
}

class ListSubscriber<T>(private val continuation: Continuation<List<T>>) : Subscriber<T> {
    private val result = mutableListOf<T>()

    override fun onSubscribe(subscription: Subscription) {
        subscription.request(Long.MAX_VALUE)
    }

    override fun onNext(document: T) {
        result.add(document)
    }

    override fun onError(error: Throwable) {
        continuation.resumeWithException(error)
    }

    override fun onComplete() {
        continuation.resume(result)
    }
}

class SingleSubscriber<T>(private val continuation: Continuation<T?>) : Subscriber<T> {
    private var result: T? = null

    override fun onSubscribe(subscription: Subscription) {
        subscription.request(1)
    }

    override fun onNext(document: T) {
        result = document
    }

    override fun onError(error: Throwable) {
        continuation.resumeWithException(error)
    }

    override fun onComplete() {
        continuation.resume(result)
    }
}

class UnitSubscriber<T>(private val continuation: Continuation<Unit>) : Subscriber<T> {
    override fun onSubscribe(subscription: Subscription) {
        subscription.request(1)
    }

    override fun onNext(document: T) {
        // no operation
    }

    override fun onError(error: Throwable) {
        continuation.resumeWithException(error)
    }

    override fun onComplete() {
        continuation.resume(Unit)
    }
}
