package com.manic.galaxy.infrastructure.ktor.galaxies

import com.manic.galaxy.infrastructure.ktor.Environment
import io.ktor.routing.*

fun Route.configureGalaxies(environment: Environment) {
    configureGalaxies(environment.galaxyService)
    configureItems(environment.itemService)
    configureGoods(environment.goodsService)
    configureSystems(environment.systemService)
    configureSpaceStations(environment.spaceStationService)
}
