package com.manic.galaxy.infrastructure.jackson

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import com.manic.galaxy.domain.system.SystemName
import com.manic.galaxy.domain.user.Email

sealed class JacksonEmail {
    class Serializer : StdSerializer<Email>(Email::class.java) {
        override fun serialize(value: Email, gen: JsonGenerator, provider: SerializerProvider) {
            gen.writeString(value.value)
        }
    }

    class Deserializer : StdDeserializer<Email>(Email::class.java) {
        override fun deserialize(p: JsonParser, ctxt: DeserializationContext): Email {
            return Email(p.text)
        }

    }
}

sealed class JacksonSystemName {
    class Serializer : StdSerializer<SystemName>(SystemName::class.java) {
        override fun serialize(value: SystemName, gen: JsonGenerator, provider: SerializerProvider) {
            gen.writeString(value.value)
        }
    }

    class Deserializer : StdDeserializer<SystemName>(SystemName::class.java) {
        override fun deserialize(p: JsonParser, ctxt: DeserializationContext): SystemName {
            return SystemName(p.text)
        }
    }
}
