package com.manic.galaxy.infrastructure.mongodb

import com.manic.galaxy.domain.shared.Aggregate
import com.manic.galaxy.domain.shared.Slice
import com.manic.galaxy.infrastructure.event.EventService
import com.mongodb.client.model.Filters
import com.mongodb.client.model.ReplaceOneModel
import com.mongodb.client.model.ReplaceOptions
import com.mongodb.reactivestreams.client.FindPublisher
import com.mongodb.reactivestreams.client.MongoCollection
import com.mongodb.reactivestreams.client.MongoDatabase
import org.bson.conversions.Bson
import kotlin.reflect.KClass

abstract class MongoRepository(private val eventService: EventService) {
    protected fun <T : Aggregate> getCollection(database: MongoDatabase, klass: KClass<in T>): MongoCollection<T> {
        return database.getCollection(klass.simpleName!!.decapitalize(), klass.java) as MongoCollection<T>
    }

    protected suspend fun <T : Aggregate> save(collection: MongoCollection<T>, aggregate: T): T {
        collection.replaceOne(Filters.eq("_id", aggregate.id), aggregate, ReplaceOptions().upsert(true)).toUnit()
        eventService.publish(aggregate.events)
        return aggregate
    }

    protected suspend fun <T : Aggregate> save(collection: MongoCollection<T>, aggregates: Collection<T>): Collection<T> {
        if (aggregates.isEmpty()) {
            return aggregates
        }
        val writeModels = aggregates.map { ReplaceOneModel(Filters.eq("_id", it.id), it, ReplaceOptions().upsert(true)) }
        collection.bulkWrite(writeModels).toUnit()
        aggregates.forEach { eventService.publish(it.events) }
        return aggregates
    }

    protected suspend fun delete(collection: MongoCollection<*>, aggregate: Aggregate) {
        collection.deleteOne(Filters.eq("_id", aggregate.id)).toUnit()
    }

    protected suspend fun count(collection: MongoCollection<*>, vararg filters: Bson?): Long {
        val nonNullFilters = filters.filterNotNull()
        return when (nonNullFilters.size) {
            0 -> collection.countDocuments()
            1 -> collection.countDocuments(nonNullFilters[0])
            else -> collection.countDocuments(Filters.and(nonNullFilters))
        }.toFirst()
    }

    protected fun <T : Aggregate> find(
        collection: MongoCollection<T>,
        slice: Slice = Slice(),
        vararg filters: Bson?
    ): FindPublisher<T> {
        val nonNullFilters = filters.filterNotNull()
        val findIterable = when (nonNullFilters.size) {
            0 -> collection.find()
            1 -> collection.find(nonNullFilters[0])
            else -> collection.find(Filters.and(nonNullFilters))
        }
        return findIterable.limit(slice.limit).skip(slice.offset)
    }

    protected suspend fun <T : Aggregate> findAll(collection: MongoCollection<T>, vararg filters: Bson?): Iterable<T> {
        val nonNullFilters = filters.filterNotNull()
        return when (nonNullFilters.size) {
            0 -> collection.find()
            1 -> collection.find(nonNullFilters[0])
            else -> collection.find(Filters.and(nonNullFilters))
        }.toList()
    }

    protected suspend fun <T : Aggregate> find(collection: MongoCollection<T>, vararg filters: Bson?): T? {
        val nonNullFilters = filters.filterNotNull()
        val findIterable = when (nonNullFilters.size) {
            0 -> collection.find()
            1 -> collection.find(nonNullFilters[0])
            else -> collection.find(Filters.and(nonNullFilters))
        }
        return findIterable.limit(1).toFirst()
    }
}
