package com.manic.galaxy.infrastructure.mongodb

import com.manic.galaxy.domain.user.Email
import com.manic.galaxy.domain.user.User
import com.manic.galaxy.domain.user.UserRepository
import com.manic.galaxy.infrastructure.event.EventService
import com.mongodb.client.model.Filters
import com.mongodb.client.model.Indexes
import com.mongodb.reactivestreams.client.MongoDatabase
import java.util.*

class MongoUserRepository(database: MongoDatabase, eventService: EventService) : MongoRepository(eventService),
    UserRepository {
    private val collection = getCollection(database, User::class)

    init {
        collection.createIndex(Indexes.ascending("email"))
    }

    override suspend fun save(user: User): User {
        return save(collection, user)
    }

    override suspend fun get(email: Email): User {
        val value = find(collection, Filters.eq("email", email))
        return requireNotNull(value) { "user.unknownEmail" }
    }

    override suspend fun get(id: UUID): User {
        val value = find(collection, Filters.eq("_id", id))
        return requireNotNull(value) { "user.unknownId" }
    }

    override suspend fun requireUnique(email: Email) {
        val count = count(collection, Filters.eq("email", email))
        require(count == 0L)
    }
}
