package com.manic.galaxy.infrastructure.mongock

import com.github.cloudyrock.mongock.driver.mongodb.sync.v4.driver.MongoSync4Driver
import com.github.cloudyrock.standalone.MongockStandalone
import com.mongodb.client.MongoClient

fun runMigration(client: MongoClient, databaseName: String) {
    val runner = MongockStandalone.builder()
        .setDriver(MongoSync4Driver.withDefaultLock(client, databaseName))
        .addChangeLogsScanPackage("com.manic.galaxy.infrastructure.mongock")
        .buildRunner()
    runner.execute();
}
