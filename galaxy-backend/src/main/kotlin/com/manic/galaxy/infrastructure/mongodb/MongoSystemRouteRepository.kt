package com.manic.galaxy.infrastructure.mongodb

import com.manic.galaxy.domain.shared.Slice
import com.manic.galaxy.domain.systemroute.SystemRoute
import com.manic.galaxy.domain.systemroute.SystemRouteRepository
import com.manic.galaxy.infrastructure.event.EventService
import com.mongodb.client.model.Filters
import com.mongodb.client.model.Indexes
import com.mongodb.client.model.Sorts
import com.mongodb.reactivestreams.client.MongoDatabase
import java.util.*

class MongoSystemRouteRepository(database: MongoDatabase, eventService: EventService) : MongoRepository(eventService),
    SystemRouteRepository {
    private val collection = getCollection(database, SystemRoute::class)

    init {
        collection.createIndex(
            Indexes.compoundIndex(
                Indexes.ascending("startId"),
                Indexes.ascending("duration")
            )
        )
    }

    override suspend fun save(systemRoutes: Set<SystemRoute>): Set<SystemRoute> {
        save(collection, systemRoutes)
        return systemRoutes
    }

    override suspend fun list(startId: UUID, slice: Slice): List<SystemRoute> {
        return find(collection, slice, Filters.eq("startId", startId))
            .sort(Sorts.ascending("duration"))
            .toList()
    }

    override suspend fun get(startId: UUID, destinationId: UUID): SystemRoute {
        return requireNotNull(
            find(
                collection,
                Filters.eq("startId", startId),
                Filters.eq("destinationId", destinationId)
            )
        ) { "systemRoute.unknownId" }
    }
}
