package com.manic.galaxy.infrastructure.mongodb

import com.manic.galaxy.domain.shared.Slice
import com.manic.galaxy.domain.system.System
import com.manic.galaxy.domain.system.SystemName
import com.manic.galaxy.domain.system.SystemRepository
import com.manic.galaxy.infrastructure.event.EventService
import com.mongodb.client.model.Aggregates
import com.mongodb.client.model.Filters
import com.mongodb.client.model.Indexes
import com.mongodb.reactivestreams.client.MongoDatabase
import java.util.*

class MongoSystemRepository(database: MongoDatabase, eventService: EventService) : MongoRepository(eventService),
    SystemRepository {
    private val collection = getCollection(database, System::class)

    init {
        collection.createIndex(
            Indexes.compoundIndex(
                Indexes.ascending("galaxyId"),
                Indexes.ascending("name"),
            )
        )
    }

    override suspend fun requireUnique(galaxyId: UUID, names: Set<SystemName>) {
        val count = count(collection, Filters.eq("galaxyId", galaxyId), Filters.all("name", names))
        require(count == 0L) { "system.takenName" }
    }

    override suspend fun save(systems: Set<System>): Set<System> {
        save(collection, systems)
        return systems
    }

    override suspend fun list(galaxyId: UUID, slice: Slice): List<System> {
        return find(collection, slice, Filters.eq("galaxyId", galaxyId)).toList()
    }

    override suspend fun list(slice: Slice, ids: List<UUID>?): List<System> {
        return find(collection, slice, ids?.let { Filters.`in`("_id", it) }).toList()
    }

    override suspend fun randomInhabited(galaxyId: UUID): System {
        val system = collection.aggregate(
            listOf(
                Aggregates.match(
                    Filters.and(
                        Filters.eq("galaxyId", galaxyId),
                        Filters.eq("inhabited", true)
                    )
                ),
                Aggregates.sample(1)
            )
        ).toList().firstOrNull()
        return requireNotNull(system) { "system.noSystemFound" }
    }

    override suspend fun get(id: UUID): System {
        return requireNotNull(find(collection, Filters.eq("_id", id))) { "system.unknownId" }
    }
}
