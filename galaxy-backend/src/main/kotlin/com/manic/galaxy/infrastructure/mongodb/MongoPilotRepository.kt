package com.manic.galaxy.infrastructure.mongodb

import com.manic.galaxy.domain.pilot.Pilot
import com.manic.galaxy.domain.pilot.PilotRepository
import com.manic.galaxy.domain.shared.GalaxyTime
import com.manic.galaxy.domain.shared.Slice
import com.manic.galaxy.infrastructure.event.EventService
import com.mongodb.client.model.Filters
import com.mongodb.client.model.Indexes
import com.mongodb.reactivestreams.client.MongoCollection
import com.mongodb.reactivestreams.client.MongoDatabase
import java.util.*

class MongoPilotRepository(database: MongoDatabase, eventService: EventService) : MongoRepository(eventService),
    PilotRepository {
    private val collection: MongoCollection<Pilot> = database.getCollection("pilot", Pilot::class.java)

    init {
        collection.createIndex(Indexes.compoundIndex(
            Indexes.ascending("userId"),
            Indexes.ascending("galaxyId"),
        ))

        collection.createIndex(Indexes.ascending("flyingDestination.arrivalAt"))
    }

    override suspend fun requireUnique(galaxyId: UUID, userId: UUID) {
        val count = count(
            collection,
            Filters.eq("userId", userId),
            Filters.eq("galaxyId", galaxyId)
        )
        require(count == 0L) { "pilot.notUnique" }
    }

    override suspend fun list(userId: UUID, slice: Slice): List<Pilot> {
        return find(collection, slice, Filters.eq("userId", userId)).toList()
    }

    override suspend fun save(pilot: Pilot): Pilot {
        return save(collection, pilot)
    }

    override suspend fun save(pilots: List<Pilot>): List<Pilot> {
        return save(collection, pilots).toList()
    }

    override suspend fun get(id: UUID): Pilot {
        return requireNotNull(find(collection, Filters.eq("_id", id))) { "pilot.unknownId" }
    }

    suspend fun findArrived(): Iterable<Pilot> {
        return findAll(collection, Filters.lte("flyingDestination.arrivalAt", GalaxyTime.now()))
    }
}
