package com.manic.galaxy.infrastructure.ktor

import com.manic.galaxy.application.*
import com.manic.galaxy.domain.pilot.PilotRepository
import com.manic.galaxy.domain.systemroute.SystemRouter
import com.manic.galaxy.infrastructure.bcrypt.BCryptPasswordEncryptor
import com.manic.galaxy.infrastructure.event.*
import com.manic.galaxy.infrastructure.jwt.JwtAccessTokenSigner
import com.manic.galaxy.infrastructure.mongodb.*
import com.mongodb.reactivestreams.client.MongoDatabase

fun createEnvironment(
    mongoConnectionString: String,
    mongoDatabaseName: String,
    eventService: EventService
): Environment {
    val database = createDatabase(mongoConnectionString, mongoDatabaseName)
    val jwtAccessTokenSigner = JwtAccessTokenSigner()
    val bCryptPasswordEncryptor = BCryptPasswordEncryptor()

    val mongoUserRepository = MongoUserRepository(database, eventService)
    val mongoRefreshTokenRepository = MongoRefreshTokenRepository(database, eventService)
    val mongoPilotRepository = MongoPilotRepository(database, eventService)
    val mongoGalaxyRepository = MongoGalaxyRepository(database, eventService)
    val mongoSystemRepository = MongoSystemRepository(database, eventService)
    val mongoSystemRouteRepository = MongoSystemRouteRepository(database, eventService)
    val mongoGoodsRepository = MongoGoodsRepository(database, eventService)
    val mongoItemRepository = MongoItemRepository(database, eventService)
    val mongoCargoRepository = MongoCargoRepository(database, eventService)
    val mongoSpaceStationRepository = MongoSpaceStationRepository(database, eventService)

    val userService = UserService(
        mongoUserRepository,
        mongoRefreshTokenRepository,
        jwtAccessTokenSigner,
        bCryptPasswordEncryptor,
        mongoPilotRepository
    )
    val goodsService = GoodsService(mongoItemRepository, mongoGoodsRepository, mongoSpaceStationRepository)
    val tradingService = TradingService(
        mongoPilotRepository,
        mongoItemRepository,
        mongoGoodsRepository,
        mongoCargoRepository
    )
    val galaxyService = GalaxyService(mongoGalaxyRepository)
    val systemService = SystemService(mongoSystemRepository)
    val systemRouteService = SystemRouteService(mongoSystemRepository, mongoSystemRouteRepository, SystemRouter())
    val cargoService = CargoService(
        mongoPilotRepository,
        mongoCargoRepository,
    )
    val itemService = ItemService(mongoItemRepository)
    val spaceStationService = SpaceStationService(
        mongoSpaceStationRepository,
        mongoSystemRepository,
    )
    val pilotService = PilotService(
        mongoPilotRepository,
        mongoSystemRepository,
        mongoSystemRouteRepository,
        mongoSpaceStationRepository
    )

    ItemSubscriber(eventService, itemService)
    GoodsSubscriber(eventService, goodsService)
    SpaceStationSubscriber(eventService, spaceStationService)
    SystemSubscriber(eventService, systemService, systemRouteService)

    return Environment(
        mongoPilotRepository,
        userService,
        galaxyService,
        systemService,
        pilotService,
        cargoService,
        itemService,
        systemRouteService,
        spaceStationService,
        goodsService,
        tradingService,
        jwtAccessTokenSigner,
        database
    )
}

data class Environment(
    val pilotRepository: PilotRepository,
    val userService: UserService,
    val galaxyService: GalaxyService,
    val systemService: SystemService,
    val pilotService: PilotService,
    val cargoService: CargoService,
    val itemService: ItemService,
    val systemRouteService: SystemRouteService,
    val spaceStationService: SpaceStationService,
    val goodsService: GoodsService,
    val tradingService: TradingService,
    val jwtAccessTokenSigner: JwtAccessTokenSigner,
    val database: MongoDatabase
)
