package com.manic.galaxy.infrastructure.mongodb

import com.manic.galaxy.domain.goods.Goods
import com.manic.galaxy.domain.goods.GoodsRepository
import com.manic.galaxy.domain.shared.Slice
import com.manic.galaxy.infrastructure.event.EventService
import com.mongodb.client.model.Filters
import com.mongodb.client.model.Indexes
import com.mongodb.client.model.Sorts
import com.mongodb.reactivestreams.client.MongoCollection
import com.mongodb.reactivestreams.client.MongoDatabase
import java.util.*

class MongoGoodsRepository(database: MongoDatabase, eventService: EventService) : MongoRepository(eventService),
    GoodsRepository {
    private val collection: MongoCollection<Goods> = getCollection(database, Goods::class)

    init {
        collection.createIndex(Indexes.ascending("galaxyId"))

        collection.createIndex(
            Indexes.compoundIndex(
                Indexes.ascending("spaceStationId"),
                Indexes.ascending("itemId"),
            )
        )

        collection.createIndex(
            Indexes.compoundIndex(
                Indexes.ascending("itemId"),
                Indexes.descending("priceMultiplier"),
            )
        )
    }

    override suspend fun save(goods: List<Goods>): List<Goods> {
        save(collection, goods)
        return goods
    }

    override suspend fun list(spaceStationId: UUID, slice: Slice): List<Goods> {
        return find(collection, slice, Filters.eq("spaceStationId", spaceStationId)).toList()
    }

    override suspend fun get(spaceStationId: UUID, itemId: UUID): Goods {
        return requireNotNull(
            find(
                collection,
                Filters.eq("spaceStationId", spaceStationId),
                Filters.eq("itemId", itemId)
            )
        ) { "goods.notInStock" }
    }

    override suspend fun listByGalaxy(galaxyId: UUID, slice: Slice): List<Goods> {
        return find(collection, slice, Filters.eq("galaxyId")).sort(Sorts.descending("priceMultiplier")).toList()
    }

    override suspend fun listByItem(itemId: UUID, slice: Slice): List<Goods> {
        return find(collection, slice, Filters.eq("itemId")).sort(Sorts.descending("priceMultiplier")).toList()
    }
}
