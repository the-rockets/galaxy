package com.manic.galaxy.infrastructure.mongodb

import com.manic.galaxy.domain.shared.Slice
import com.manic.galaxy.domain.spacestation.SpaceStation
import com.manic.galaxy.domain.spacestation.SpaceStationRepository
import com.manic.galaxy.infrastructure.event.EventService
import com.mongodb.client.model.Filters
import com.mongodb.client.model.Indexes
import com.mongodb.reactivestreams.client.MongoCollection
import com.mongodb.reactivestreams.client.MongoDatabase
import java.util.*

class MongoSpaceStationRepository(database: MongoDatabase, eventService: EventService) : MongoRepository(eventService),
    SpaceStationRepository {
    private val collection: MongoCollection<SpaceStation> = getCollection(database, SpaceStation::class)

    init {
        collection.createIndex(Indexes.ascending("systemId"))
        collection.createIndex(Indexes.ascending("galaxyId"))
    }

    override suspend fun save(spaceStation: SpaceStation): SpaceStation {
        return save(collection, spaceStation)
    }

    override suspend fun list(systemId: UUID): List<SpaceStation> {
        return find(collection, Slice(0, 0), Filters.eq("systemId", systemId)).toList()
    }

    override suspend fun list(galaxyId: UUID, slice: Slice): List<SpaceStation> {
        return find(collection, slice, Filters.eq("galaxyId", galaxyId)).toList()
    }

    override suspend fun get(spaceStationId: UUID): SpaceStation {
        return requireNotNull(find(collection, Filters.eq("_id", spaceStationId))) { "spaceStation.unknownId" }
    }
}
