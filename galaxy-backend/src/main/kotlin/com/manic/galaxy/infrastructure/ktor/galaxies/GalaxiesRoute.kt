package com.manic.galaxy.infrastructure.ktor.galaxies

import com.manic.galaxy.application.GalaxyService
import com.manic.galaxy.domain.shared.Slice
import com.manic.galaxy.infrastructure.ktor.toUUID
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.response.*
import io.ktor.routing.*
import java.util.*

fun Route.configureGalaxies(galaxyService: GalaxyService) = authenticate {
    get("/api/galaxies") {
        val limit: Int? = call.request.queryParameters["limit"]?.toInt()
        val offset: Int? = call.request.queryParameters["offset"]?.toInt()
        val ids: List<UUID>? = call.request.queryParameters.getAll("id[]")?.map(String::toUUID)
        call.respond(galaxyService.listGalaxies(Slice(limit, offset), ids))
    }
}
