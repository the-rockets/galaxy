package com.manic.galaxy.infrastructure.event

import com.manic.galaxy.application.SystemRouteService
import com.manic.galaxy.application.SystemService
import com.manic.galaxy.domain.galaxy.GalaxyCreated
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class SystemSubscriber(
    eventService: EventService,
    private val systemService: SystemService,
    private val systemRouteService: SystemRouteService
) {
    init {
        eventService.subscribe(GalaxyCreated::class, this::onGalaxyCreated)
    }

    private suspend fun onGalaxyCreated(galaxyCreated: GalaxyCreated) = withContext(Dispatchers.IO) {
        systemService.createSystems(galaxyCreated.galaxyId, 100)
        systemRouteService.createSystemRoutes(galaxyCreated.galaxyId)
    }
}
