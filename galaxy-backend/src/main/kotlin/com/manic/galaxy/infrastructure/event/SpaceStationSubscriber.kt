package com.manic.galaxy.infrastructure.event

import com.manic.galaxy.application.SpaceStationService
import com.manic.galaxy.domain.system.SystemCreated
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class SpaceStationSubscriber(
    eventService: EventService,
    private val spaceStationService: SpaceStationService,
) {
    init {
        eventService.subscribe(SystemCreated::class, this::onSystemCreated)
    }

    private suspend fun onSystemCreated(systemCreated: SystemCreated) = withContext(Dispatchers.IO) {
        if (systemCreated.inhabited) {
            spaceStationService.createSpaceStation(systemCreated.systemId)
        }
    }
}
