package com.manic.galaxy.infrastructure.event

import com.manic.galaxy.application.ItemService
import com.manic.galaxy.domain.galaxy.GalaxyCreated
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ItemSubscriber(
    eventService: EventService,
    private val itemService: ItemService,
) {
    init {
        eventService.subscribe(GalaxyCreated::class, this::onGalaxyCreated)
    }

    private suspend fun onGalaxyCreated(galaxyCreated: GalaxyCreated) = withContext(Dispatchers.IO) {
        itemService.createItems(galaxyCreated.galaxyId)
    }
}
