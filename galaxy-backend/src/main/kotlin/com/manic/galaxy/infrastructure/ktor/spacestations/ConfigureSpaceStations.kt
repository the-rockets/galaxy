package com.manic.galaxy.infrastructure.ktor.spacestations

import com.manic.galaxy.infrastructure.ktor.Environment
import io.ktor.routing.*

fun Route.configureSpaceStations(environment: Environment) {
    configureGoods(environment.goodsService)
}
