package com.manic.galaxy.infrastructure.ktor.galaxies

import com.manic.galaxy.application.GoodsService
import com.manic.galaxy.domain.shared.Slice
import com.manic.galaxy.infrastructure.ktor.toUUID
import io.ktor.application.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.util.*
import java.util.*

fun Route.configureGoods(goodsService: GoodsService) {
    get("/api/galaxies/{id}/goods") {
        val limit: Int? = call.request.queryParameters["limit"]?.toInt()
        val offset: Int? = call.request.queryParameters["offset"]?.toInt()
        val galaxyId: UUID = call.parameters.getOrFail("id").toUUID()

        call.respond(goodsService.listGoodsByGalaxy(galaxyId, Slice(limit, offset)))
    }
}
