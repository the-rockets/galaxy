package com.manic.galaxy.infrastructure.mongodb

import com.manic.galaxy.domain.galaxy.Galaxy
import com.manic.galaxy.domain.galaxy.GalaxyRepository
import com.manic.galaxy.domain.shared.Slice
import com.manic.galaxy.infrastructure.event.EventService
import com.mongodb.client.model.Filters
import com.mongodb.client.model.Indexes
import com.mongodb.reactivestreams.client.MongoCollection
import com.mongodb.reactivestreams.client.MongoDatabase
import java.util.*

class MongoGalaxyRepository(database: MongoDatabase, eventService: EventService) : MongoRepository(eventService),
    GalaxyRepository {
    private val collection: MongoCollection<Galaxy> = getCollection(database, Galaxy::class)

    init {
        collection.createIndex(Indexes.ascending("name"))
    }

    override suspend fun requireUnique(name: String) {
        val count = count(collection, Filters.eq("name", name))
        require(count == 0L) { "galaxy.takenName" }
    }

    override suspend fun save(galaxy: Galaxy): Galaxy {
        return save(collection, galaxy)
    }

    override suspend fun list(slice: Slice, ids: List<UUID>?): List<Galaxy> {
        return find(collection, slice, ids?.let { Filters.`in`("_id", it) }).toList()
    }
}
