package com.manic.galaxy.infrastructure.event

import kotlin.reflect.KClass

interface EventService {
    suspend fun publish(events: List<Any>)
    fun <T : Any> subscribe(event: KClass<T>, callback: suspend (T) -> Unit)
}
