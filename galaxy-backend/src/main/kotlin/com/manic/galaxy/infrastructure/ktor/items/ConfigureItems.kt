package com.manic.galaxy.infrastructure.ktor.items

import com.manic.galaxy.infrastructure.ktor.Environment
import io.ktor.routing.*

fun Route.configureItems(environment: Environment) {
    configureGoods(environment.goodsService)
}

