package com.manic.galaxy.infrastructure.ktor

import java.util.*

fun String.toUUID(): UUID {
    return UUID.fromString(this)
}
