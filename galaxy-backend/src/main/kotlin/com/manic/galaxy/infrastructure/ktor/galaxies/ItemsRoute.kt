package com.manic.galaxy.infrastructure.ktor.galaxies

import com.manic.galaxy.application.ItemService
import com.manic.galaxy.domain.shared.Slice
import com.manic.galaxy.infrastructure.ktor.toUUID
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.util.*

fun Route.configureItems(itemService: ItemService) = authenticate {
    get("/api/galaxies/{id}/items") {
        val id = call.parameters.getOrFail("id").toUUID()
        val limit: Int? = call.request.queryParameters["limit"]?.toInt()
        val offset: Int? = call.request.queryParameters["offset"]?.toInt()
        call.respond(itemService.listItems(id, Slice(limit, offset)))
    }
}
