package com.manic.galaxy.infrastructure.jackson

import com.fasterxml.jackson.databind.module.SimpleModule
import com.manic.galaxy.domain.system.SystemName
import com.manic.galaxy.domain.user.Email

fun createKtorModule(): SimpleModule = SimpleModule().apply {
    addSerializer(JacksonEmail.Serializer())
    addDeserializer(Email::class.java, JacksonEmail.Deserializer())
    addSerializer(JacksonSystemName.Serializer())
    addDeserializer(SystemName::class.java, JacksonSystemName.Deserializer())
}
