package com.manic.galaxy.infrastructure.ktor.systems

import com.manic.galaxy.application.SystemRouteService
import com.manic.galaxy.domain.shared.Slice
import com.manic.galaxy.infrastructure.ktor.toUUID
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.util.*

fun Route.configureSystemRoutes(systemRouteService: SystemRouteService) = authenticate {
    get("/api/systems/{id}/routes") {
        val id = call.parameters.getOrFail("id").toUUID()
        val limit: Int? = call.request.queryParameters["limit"]?.toInt()
        val offset: Int? = call.request.queryParameters["offset"]?.toInt()
        call.respond(systemRouteService.listSystemRoutes(id, Slice(limit, offset)))
    }
}
