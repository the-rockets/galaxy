package com.manic.galaxy.infrastructure.ktor

import com.manic.galaxy.infrastructure.flow.FlowEventService
import io.ktor.application.*

/**
 * Please note that you can use any other name instead of *module*.
 * Also note that you can have more then one modules in your application.
 * */
@Suppress("unused") // Referenced in application.conf
fun Application.module() {
    val flowEventService = FlowEventService(this)
    val environment = createEnvironment("mongodb://root:root@localhost:27017", "galaxy", flowEventService)
    configureSecurity(environment)
    configureHTTP()
    configureSerialization()
    configureMonitoring()
    configureLocalhost(environment)
    configureRouting(environment, flowEventService)
    configureScheduling(environment)
}
