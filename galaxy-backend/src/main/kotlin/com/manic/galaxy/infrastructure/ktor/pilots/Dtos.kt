package com.manic.galaxy.infrastructure.ktor.pilots

import java.util.*

sealed class PostPilots {
    data class Body(
        val galaxyId: UUID,
        val name: String,
    )
}

sealed class PostFlyTo {
    data class Body(
        val systemId: UUID
    )
}


sealed interface PostDockAt {
    data class Body(
        val spaceStationId: UUID,
    )
}

sealed interface PostBuyItem {
    data class Body(
        val itemId: UUID,
        val amount: Long
    )
}

sealed interface PostSellItem {
    data class Body(
        val itemId: UUID,
        val amount: Long
    )
}
