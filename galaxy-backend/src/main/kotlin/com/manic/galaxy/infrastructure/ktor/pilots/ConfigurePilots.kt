package com.manic.galaxy.infrastructure.ktor.pilots

import com.manic.galaxy.infrastructure.ktor.Environment
import io.ktor.routing.*

fun Route.configurePilots(environment: Environment) {
    configurePilots(environment.pilotService, environment.tradingService)
    configureCargo(environment.cargoService)
}
