package com.manic.galaxy.infrastructure.ktor

import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.auth.jwt.*
import java.util.*

fun Application.configureSecurity(applicationEnvironment: Environment) {

    val jwtRealm = environment.config.property("jwt.realm").getString()

    authentication {
        jwt {
            realm = jwtRealm
            verifier(applicationEnvironment.jwtAccessTokenSigner.jwtVerifier)
            validate { credential ->
                AccessTokenPrincipal(UUID.fromString(credential.payload.subject))
            }
        }
    }

}

data class AccessTokenPrincipal(
    val userId: UUID
) : Principal
