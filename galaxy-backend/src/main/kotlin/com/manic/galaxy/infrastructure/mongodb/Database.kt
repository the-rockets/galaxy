package com.manic.galaxy.infrastructure.mongodb

import com.manic.galaxy.infrastructure.mongock.runMigration
import com.mongodb.ConnectionString
import com.mongodb.MongoClientSettings
import com.mongodb.reactivestreams.client.MongoClients
import com.mongodb.reactivestreams.client.MongoDatabase
import org.bson.UuidRepresentation
import org.bson.codecs.configuration.CodecRegistries.fromProviders
import org.bson.codecs.configuration.CodecRegistries.fromRegistries
import org.bson.codecs.pojo.Conventions
import org.bson.codecs.pojo.PojoCodecProvider


fun createDatabase(connectionString: String, databaseName: String): MongoDatabase {
    val defaultCodecs = MongoClientSettings.getDefaultCodecRegistry()
    val pojoCodec = fromProviders(
        PojoCodecProvider
            .builder()
            .register("com.manic.galaxy.domain")
            .conventions(
                listOf(
                    Conventions.CLASS_AND_PROPERTY_CONVENTION,
                    Conventions.SET_PRIVATE_FIELDS_CONVENTION
                )
            )
            .automatic(true)
            .build()
    )
    val applicationCodecs = createCodecs()
    val codecRegistry = fromRegistries(defaultCodecs, applicationCodecs, pojoCodec)
    val settings = MongoClientSettings
        .builder()
        .uuidRepresentation(UuidRepresentation.STANDARD)
        .applyConnectionString(ConnectionString(connectionString))
        .codecRegistry(codecRegistry)
        .build()
    val client = MongoClients.create(settings)

    val syncClient = com.mongodb.client.MongoClients.create(settings)
    runMigration(syncClient, databaseName)
    syncClient.close()

    return client.getDatabase(databaseName)
}
