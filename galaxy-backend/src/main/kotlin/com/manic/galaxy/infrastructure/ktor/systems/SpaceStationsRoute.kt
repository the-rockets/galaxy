package com.manic.galaxy.infrastructure.ktor.systems

import com.manic.galaxy.application.SpaceStationService
import com.manic.galaxy.infrastructure.ktor.toUUID
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.util.*
import java.util.*

internal fun Route.configureSpaceStations(spaceStationService: SpaceStationService) = authenticate {
    get("/api/systems/{id}/space-stations") {
        val systemId: UUID = call.parameters.getOrFail("id").toUUID()

        call.respond(spaceStationService.listSpaceStations(systemId))
    }
}
