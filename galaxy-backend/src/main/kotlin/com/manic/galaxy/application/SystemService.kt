package com.manic.galaxy.application

import com.manic.galaxy.domain.shared.Slice
import com.manic.galaxy.domain.system.System
import com.manic.galaxy.domain.system.SystemName
import com.manic.galaxy.domain.system.SystemRepository
import java.util.*

class SystemService(
    private val systemRepository: SystemRepository,
) {
    suspend fun createSystems(galaxyId: UUID, amount: Int) {
        val systemNames = SystemName.random(amount)
        systemRepository.requireUnique(galaxyId, systemNames)
        val systems = System.create(galaxyId, systemNames)
        systemRepository.save(systems)
    }

    suspend fun listSystems(galaxyId: UUID, slice: Slice): List<System> {
        return systemRepository.list(galaxyId, slice)
    }
}
