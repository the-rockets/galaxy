package com.manic.galaxy.application

import com.manic.galaxy.domain.shared.Slice
import com.manic.galaxy.domain.system.SystemRepository
import com.manic.galaxy.domain.systemroute.SystemRoute
import com.manic.galaxy.domain.systemroute.SystemRouteRepository
import com.manic.galaxy.domain.systemroute.SystemRouter
import java.util.*

class SystemRouteService(
    private val systemRepository: SystemRepository,
    private val systemRouteRepository: SystemRouteRepository,
    private val systemRouter: SystemRouter
) {

    suspend fun createSystemRoutes(galaxyId: UUID): Set<SystemRoute> {
        val systems = systemRepository.list(galaxyId, Slice(0, 0))
        val routes = systemRouter.routeGalaxy(systems)
        return systemRouteRepository.save(routes)
    }

    suspend fun listSystemRoutes(startId: UUID, slice: Slice): List<SystemRoute> {
        return systemRouteRepository.list(startId, slice)
    }
}
