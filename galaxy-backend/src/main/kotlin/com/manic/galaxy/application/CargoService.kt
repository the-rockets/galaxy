package com.manic.galaxy.application

import com.manic.galaxy.domain.cargo.Cargo
import com.manic.galaxy.domain.cargo.CargoRepository
import com.manic.galaxy.domain.pilot.PilotRepository
import com.manic.galaxy.domain.shared.Slice
import java.util.*

class CargoService(
    private val pilotRepository: PilotRepository,
    private val cargoRepository: CargoRepository
) {
    suspend fun listCargo(userId: UUID, pilotId: UUID, slice: Slice): List<Cargo> {
        val pilot = pilotRepository.get(pilotId)
        pilot.requireAuthorized(userId)

        return cargoRepository.list(pilotId, slice)
    }
}
