package com.manic.galaxy.application

import com.manic.galaxy.domain.cargo.Cargo
import com.manic.galaxy.domain.cargo.CargoRepository
import com.manic.galaxy.domain.goods.GoodsRepository
import com.manic.galaxy.domain.item.ItemRepository
import com.manic.galaxy.domain.pilot.Pilot
import com.manic.galaxy.domain.pilot.PilotRepository
import java.util.*

class TradingService(
    private val pilotRepository: PilotRepository,
    private val itemRepository: ItemRepository,
    private val goodsRepository: GoodsRepository,
    private val cargoRepository: CargoRepository,
) {

    /**
     * Buys an item and stores it immediately into the pilots cargo.
     *
     * Invariants:
     * - requires the pilot to be docked
     * - requires enough credits
     * - requires enough cargo space
     */
    suspend fun buyItem(userId: UUID, pilotId: UUID, itemId: UUID, amount: Long): Pilot {
        val pilot = pilotRepository.get(pilotId)
        pilot.requireAuthorized(userId)
        pilot.requireDocked()

        val goods = goodsRepository.get(pilot.spaceStationId!!, itemId)
        val item = itemRepository.get(itemId)

        val basePrice = item.calculateBasePrice(amount)
        val price = goods.calculatePrice(basePrice)
        val volume = item.calculateVolume(amount)

        val editedPilot = pilot
            .withdrawCredits(price)
            .storeCargo(volume)

        val cargo = cargoRepository
            .find(pilotId, itemId)
            ?.add(amount)
            ?: Cargo.create(pilot.id, itemId, volume, amount)

        cargoRepository.save(cargo)
        return pilotRepository.save(editedPilot)
    }

    /**
     * Sells an item from the pilots cargo.
     *
     * Invariants:
     * - requires the pilot to be docked
     */
    suspend fun sellItem(userId: UUID, pilotId: UUID, itemId: UUID, amount: Long): Pilot {
        val pilot = pilotRepository.get(pilotId)
        pilot.requireAuthorized(userId)
        pilot.requireDocked()

        val goods = goodsRepository.get(pilot.spaceStationId!!, itemId)
        val item = itemRepository.get(itemId)

        val basePrice = item.calculateBasePrice(amount)
        val price = goods.calculatePrice(basePrice)
        val volume = item.calculateVolume(amount)

        val editedPilot = pilot
            .depositCredits(price)
            .removeCargo(volume)

        val cargo = cargoRepository
            .get(pilotId, itemId)
            .remove(amount)

        when {
            cargo.isEmpty() -> cargoRepository.delete(cargo)
            else -> cargoRepository.save(cargo)
        }
        return pilotRepository.save(editedPilot)
    }
}
