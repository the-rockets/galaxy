package com.manic.galaxy.application

import com.manic.galaxy.domain.item.Item
import com.manic.galaxy.domain.item.ItemRepository
import com.manic.galaxy.domain.shared.Slice
import java.util.*

class ItemService(
    private val itemRepository: ItemRepository
) {

    suspend fun listItems(galaxyId: UUID, slice: Slice): List<Item> {
        return itemRepository.list(galaxyId, slice)
    }

    suspend fun createItems(galaxyId: UUID) {
        val items: Set<Item> = Item.create(galaxyId)
        itemRepository.save(items)
    }
}
