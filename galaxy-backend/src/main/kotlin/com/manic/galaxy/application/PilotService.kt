package com.manic.galaxy.application

import com.manic.galaxy.domain.pilot.Pilot
import com.manic.galaxy.domain.pilot.PilotRepository
import com.manic.galaxy.domain.shared.Slice
import com.manic.galaxy.domain.spacestation.SpaceStationRepository
import com.manic.galaxy.domain.system.SystemRepository
import com.manic.galaxy.infrastructure.mongodb.MongoSystemRouteRepository
import java.util.*

class PilotService(
    private val pilotRepository: PilotRepository,
    private val systemRepository: SystemRepository,
    private val systemRouteRepository: MongoSystemRouteRepository,
    private val spaceStationRepository: SpaceStationRepository,
) {

    /**
     * Creates a pilot in a random inhabited system.
     */
    suspend fun createPilot(userId: UUID, galaxyId: UUID, name: String): Pilot {
        pilotRepository.requireUnique(galaxyId, userId)
        val system = systemRepository.randomInhabited(galaxyId)
        return pilotRepository.save(Pilot.create(userId, galaxyId, system.id, name))
    }

    suspend fun listPilots(userId: UUID, slice: Slice): List<Pilot> {
        return pilotRepository.list(userId, slice)
    }

    suspend fun flyTo(userId: UUID, pilotId: UUID, systemId: UUID): Pilot {
        val pilot = pilotRepository.get(pilotId)
        pilot.requireAuthorized(userId)

        val route = systemRouteRepository.get(pilot.systemId, systemId)
        val pilotWithFlyingDestination = pilot.flyTo(route.destinationId, route.duration)
        return pilotRepository.save(pilotWithFlyingDestination)
    }

    suspend fun dockAt(userId: UUID, pilotId: UUID, spaceStationId: UUID): Pilot {
        val pilot = pilotRepository.get(pilotId)
        pilot.requireAuthorized(userId)

        val spaceStation = spaceStationRepository.get(spaceStationId)
        val route = systemRouteRepository.get(pilot.systemId, spaceStation.systemId)
        val pilotWithDestination = pilot.dockAt(spaceStation.id, route.destinationId, route.duration)
        return pilotRepository.save(pilotWithDestination)
    }

    suspend fun reachDestinations(pilotId: UUID): Pilot {
        val pilot = pilotRepository.get(pilotId)
        pilot.reachDestination()
        return pilotRepository.save(pilot)
    }

    suspend fun get(userId: UUID, pilotId: UUID): Pilot {
        val pilot = pilotRepository.get(pilotId)
        pilot.requireAuthorized(userId)

        return pilot
    }
}
