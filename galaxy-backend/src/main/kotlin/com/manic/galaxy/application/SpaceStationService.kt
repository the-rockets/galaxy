package com.manic.galaxy.application

import com.manic.galaxy.domain.shared.Slice
import com.manic.galaxy.domain.spacestation.SpaceStation
import com.manic.galaxy.domain.spacestation.SpaceStationRepository
import com.manic.galaxy.domain.system.SystemRepository
import java.util.*

class SpaceStationService(
    private val spaceStationRepository: SpaceStationRepository,
    private val systemRepository: SystemRepository,
) {
    suspend fun createSpaceStation(systemId: UUID): SpaceStation {
        val system = systemRepository.get(systemId)
        system.requireInhabited()

        val spaceStation = SpaceStation.create(system.galaxyId, system.id, system.name.value + "-Station")
        return spaceStationRepository.save(spaceStation)
    }

    suspend fun listSpaceStations(systemId: UUID): List<SpaceStation> {
        return spaceStationRepository.list(systemId)
    }

    suspend fun listSpaceStations(galaxyId: UUID, slice: Slice): List<SpaceStation> {
        return spaceStationRepository.list(galaxyId, slice)
    }
}
