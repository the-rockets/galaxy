package com.manic.galaxy.application

import com.manic.galaxy.domain.goods.Goods
import com.manic.galaxy.domain.goods.GoodsRepository
import com.manic.galaxy.domain.item.Item
import com.manic.galaxy.domain.item.ItemRepository
import com.manic.galaxy.domain.item.ItemType
import com.manic.galaxy.domain.shared.Slice
import com.manic.galaxy.domain.spacestation.SpaceStationRepository
import java.util.*

class GoodsService(
    private val itemRepository: ItemRepository,
    private val goodsRepository: GoodsRepository,
    private val spaceStationRepository: SpaceStationRepository,
) {

    /**
     * Creates goods for the space station.
     *
     * - Each station has food items
     * - the price is determined by the food demand of the station
     */
    suspend fun createGoods(spaceStationId: UUID) {
        val spaceStation = spaceStationRepository.get(spaceStationId)
        val foodItems = itemRepository.find(ItemType.FOOD).map(Item::id).toSet()
        val foodDemand = spaceStation.getFoodDemand()
        val goods = Goods.createFood(spaceStation.galaxyId, spaceStation.id, foodItems, foodDemand)
        goodsRepository.save(goods)
    }

    suspend fun listGoods(spaceStationId: UUID, slice: Slice): List<Goods> {
        return goodsRepository.list(spaceStationId, slice)
    }

    suspend fun listGoodsByGalaxy(galaxyId: UUID, slice: Slice): List<Goods> {
        return goodsRepository.listByGalaxy(galaxyId, slice)
    }

    suspend fun listGoodsByItem(itemId: UUID, slice: Slice): List<Goods> {
        return goodsRepository.listByItem(itemId, slice)
    }
}
