package com.manic.galaxy.application

import com.manic.galaxy.domain.pilot.PilotRepository
import com.manic.galaxy.domain.refreshtoken.AccessTokenSigner
import com.manic.galaxy.domain.refreshtoken.RefreshToken
import com.manic.galaxy.domain.refreshtoken.RefreshTokenRepository
import com.manic.galaxy.domain.refreshtoken.SignedAccessToken
import com.manic.galaxy.domain.user.Email
import com.manic.galaxy.domain.user.PasswordEncryptor
import com.manic.galaxy.domain.user.User
import com.manic.galaxy.domain.user.UserRepository
import java.util.*

class UserService(
    private val userRepository: UserRepository,
    private val refreshTokenRepository: RefreshTokenRepository,
    private val accessTokenSigner: AccessTokenSigner,
    private val passwordEncryptor: PasswordEncryptor,
    private val pilotRepository: PilotRepository
) {

    suspend fun createUser(email: Email, rawPassword: String): User {
        userRepository.requireUnique(email)
        val password = passwordEncryptor.encrypt(rawPassword)
        val user = User.create(email, password)
        return userRepository.save(user)
    }

    suspend fun signIn(email: Email, rawPassword: String): RefreshToken {
        val user = userRepository.get(email)
        passwordEncryptor.requireMatch(user.password, rawPassword)

        val refreshToken = user.signIn()
        return refreshTokenRepository.save(refreshToken)
    }

    suspend fun access(refreshTokenId: UUID): SignedAccessToken {
        val refreshToken = refreshTokenRepository.get(refreshTokenId)
        val refreshedRefreshToken = refreshToken.refresh()
        val accessToken = refreshToken.access()

        refreshTokenRepository.save(refreshedRefreshToken)
        return accessTokenSigner.sign(accessToken)
    }

    suspend fun signOut(refreshTokenId: UUID) {
        val refreshToken = refreshTokenRepository.get(refreshTokenId)
        refreshTokenRepository.delete(refreshToken)
    }

    suspend fun getUser(userId: UUID): User {
        return userRepository.get(userId)
    }

    suspend fun selectPilot(userId: UUID, pilotId: UUID): User {
        val pilot = pilotRepository.get(pilotId)
        pilot.requireAuthorized(userId)

        val user = userRepository.get(userId)

        return userRepository.save(user.selectPilot(pilot.id))
    }
}
