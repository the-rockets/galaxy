package com.manic.galaxy.application

import com.manic.galaxy.domain.galaxy.Galaxy
import com.manic.galaxy.domain.galaxy.GalaxyRepository
import com.manic.galaxy.domain.shared.Slice
import java.util.*

class GalaxyService(
    private val galaxyRepository: GalaxyRepository
) {
    suspend fun createGalaxy(name: String): Galaxy {
        galaxyRepository.requireUnique(name)
        return galaxyRepository.save(Galaxy.create(name))
    }

    suspend fun listGalaxies(slice: Slice, ids: List<UUID>? = null): List<Galaxy> {
        return galaxyRepository.list(slice, ids)
    }
}
