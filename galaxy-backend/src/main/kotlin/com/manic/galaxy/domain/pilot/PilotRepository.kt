package com.manic.galaxy.domain.pilot

import com.manic.galaxy.domain.shared.Slice
import java.util.*

interface PilotRepository {
    suspend fun requireUnique(galaxyId: UUID, userId: UUID)
    suspend fun save(pilot: Pilot): Pilot
    suspend fun save(pilots: List<Pilot>): List<Pilot>
    suspend fun list(userId: UUID, slice: Slice): List<Pilot>
    suspend fun get(id: UUID): Pilot
}
