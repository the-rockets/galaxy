package com.manic.galaxy.domain.cargo

import com.manic.galaxy.domain.shared.Slice
import java.util.*

interface CargoRepository {
    suspend fun save(cargo: Cargo): Cargo
    suspend fun find(pilotId: UUID, itemId: UUID): Cargo?
    suspend fun get(pilotId: UUID, itemId: UUID): Cargo
    suspend fun delete(cargo: Cargo)
    suspend fun list(pilotId: UUID, slice: Slice): List<Cargo>
}
