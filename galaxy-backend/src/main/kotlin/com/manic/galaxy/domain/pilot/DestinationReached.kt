package com.manic.galaxy.domain.pilot

import java.util.*

data class DestinationReached(val userId: UUID, val pilotId: UUID, val systemId: UUID)
