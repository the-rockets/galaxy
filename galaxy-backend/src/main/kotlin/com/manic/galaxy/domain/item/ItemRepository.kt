package com.manic.galaxy.domain.item

import com.manic.galaxy.domain.shared.Slice
import java.util.*

interface ItemRepository {
    suspend fun save(items: Set<Item>): Set<Item>
    suspend fun get(itemId: UUID): Item
    suspend fun list(galaxyId: UUID, slice: Slice): List<Item>
    suspend fun find(itemType: ItemType): List<Item>
}
