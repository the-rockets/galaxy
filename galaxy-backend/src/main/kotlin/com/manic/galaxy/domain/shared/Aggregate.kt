package com.manic.galaxy.domain.shared

import com.fasterxml.jackson.annotation.JsonIgnore
import java.util.*

@NoArgConstructor
abstract class Aggregate() {
    abstract val id: UUID
    @Transient
    @JsonIgnore
    val events: List<Any> = mutableListOf()


    protected fun addEvent(event: Any) {
        (events as MutableList<Any>).add(event)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Aggregate

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }
}
