package com.manic.galaxy.domain.shared

data class Probability(private val value: Double) {
    fun next(): Boolean = Math.random() < value
}
