package com.manic.galaxy.domain.galaxy

import com.manic.galaxy.domain.shared.Aggregate
import java.util.*

data class Galaxy(
    val name: String,
    override val id: UUID,
) : Aggregate() {
    companion object {
        fun create(name: String): Galaxy {
            val galaxy = Galaxy(name, UUID.randomUUID())
            galaxy.addEvent(GalaxyCreated(galaxy.id, galaxy.name))
            return galaxy
        }
    }
}
