package com.manic.galaxy.domain.galaxy

import com.manic.galaxy.domain.shared.Slice
import java.util.*

interface GalaxyRepository {
    suspend fun requireUnique(name: String)
    suspend fun save(galaxy: Galaxy): Galaxy
    suspend fun list(slice: Slice, ids: List<UUID>?): List<Galaxy>
}
