package com.manic.galaxy.domain.pilot

import com.manic.galaxy.domain.shared.Aggregate
import com.manic.galaxy.domain.shared.GalaxyTime
import java.time.Duration
import java.util.*

data class Pilot(
    val userId: UUID,
    val galaxyId: UUID,
    val systemId: UUID,
    val spaceStationId: UUID?,
    val name: String,
    val flyingDestination: FlyingDestination?,
    val storageCapacity: Long,
    val storageVolume: Long,
    val credits: Long,
    override val id: UUID,
) : Aggregate() {

    fun flyTo(systemId: UUID, travelTime: Duration): Pilot {
        return copy(
            spaceStationId = null,
            flyingDestination = FlyingDestination(systemId, null, GalaxyTime.now().plus(travelTime))
        )
    }

    fun requireAuthorized(userId: UUID) {
        require(userId == this.userId) { "pilot.accessNotAllowed" }
    }

    fun reachDestination(): Pilot {
        requireNotNull(flyingDestination) { "pilot.noDestination" }
        require(flyingDestination.arrivalAt <= GalaxyTime.now()) { "pilot.notArrivedDestination" }
        return copy(
            systemId = flyingDestination.systemId,
            spaceStationId = flyingDestination.spaceStationId,
            flyingDestination = null
        ).apply { addEvent(DestinationReached(userId, id, systemId)) }
    }

    fun dockAt(spaceStationId: UUID, systemId: UUID, duration: Duration): Pilot {
        return when (duration) {
            Duration.ZERO -> copy(
                spaceStationId = spaceStationId,
                flyingDestination = null
            )
            else -> copy(
                spaceStationId = null,
                flyingDestination = FlyingDestination(
                    systemId,
                    spaceStationId,
                    GalaxyTime.now().plus(duration)
                )
            )
        }
    }

    fun depositCredits(credits: Long): Pilot {
        return copy(
            credits = this.credits + credits,
        )
    }

    fun withdrawCredits(credits: Long): Pilot {
        require(this.credits >= credits) { "pilot.notEnoughCredits" }
        return copy(
            credits = this.credits - credits,
        )
    }

    fun removeCargo(volume: Long): Pilot {
        requireNotNull(spaceStationId) { "pilot.notDocked" }

        return copy(
            storageVolume = storageVolume - volume
        )
    }

    fun storeCargo(volume: Long): Pilot {
        val newStorageVolume = storageVolume + volume
        require(newStorageVolume <= storageCapacity) { "pilot.notEnoughCapacity" }
        requireNotNull(spaceStationId) { "pilot.notDocked" }

        return copy(
            storageVolume = newStorageVolume
        )
    }

    fun requireDocked() {
        requireNotNull(spaceStationId) { "pilot.notDocked" }
    }

    companion object {
        fun create(userId: UUID, galaxyId: UUID, systemId: UUID, name: String): Pilot {
            require(name.isNotBlank()) { "pilot.invalidName" }

            return Pilot(
                userId,
                galaxyId,
                systemId,
                null,
                name,
                null,
                100,
                0,
                1000,
                UUID.randomUUID()
            )
        }
    }
}

