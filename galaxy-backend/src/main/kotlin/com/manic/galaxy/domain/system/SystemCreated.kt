package com.manic.galaxy.domain.system

import java.util.*

data class SystemCreated(
    val galaxyId: UUID,
    val systemId: UUID,
    val name: SystemName,
    val neighborSystemIds: List<UUID>,
    val inhabited: Boolean,
) {
    companion object {
        fun of(system: System): SystemCreated {
            return SystemCreated(
                system.galaxyId,
                system.id,
                system.name,
                system.neighborSystemIds,
                system.inhabited
            )
        }
    }
}
