package com.manic.galaxy.domain.pilot

import com.manic.galaxy.domain.shared.ValueObject
import java.time.Instant
import java.util.*

data class FlyingDestination(
    val systemId: UUID,
    val spaceStationId: UUID?,
    val arrivalAt: Instant
) : ValueObject()
