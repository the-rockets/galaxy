package com.manic.galaxy.domain.spacestation

import com.manic.galaxy.domain.shared.Aggregate
import java.util.*

data class SpaceStation(
    val galaxyId: UUID,
    val systemId: UUID,
    val name: String,
    override val id: UUID
) : Aggregate() {
    fun getFoodDemand(): Double {
        val range = 5..12
        val random = range.random() / 10.0
        return 1.0 / random
    }

    companion object {
        fun create(galaxyId: UUID, systemId: UUID, name: String): SpaceStation {
            return SpaceStation(galaxyId, systemId, name, UUID.randomUUID())
                .apply { addEvent(SpaceStationCreated(galaxyId, systemId, name, id)) }
        }
    }
}
