package com.manic.galaxy.domain.user

import com.manic.galaxy.domain.refreshtoken.RefreshToken
import com.manic.galaxy.domain.shared.Aggregate
import com.manic.galaxy.domain.shared.NoArgConstructor
import java.util.*

@NoArgConstructor
data class User(
    val email: Email,
    val password: Password,
    val selectedPilotId: UUID?,
    override val id: UUID,
) : Aggregate() {
    fun signIn(): RefreshToken {
        return RefreshToken(id, id = UUID.randomUUID())
    }

    fun requireAuthorized(userId: UUID) {
        require(this.id == userId) { "user.accessNotAllowed" }
    }

    fun selectPilot(pilotId: UUID): User {
        return copy(selectedPilotId = pilotId)
    }

    companion object {
        fun create(email: Email, password: Password): User {
            return User(email, password, null, UUID.randomUUID())
        }
    }
}
