package com.manic.galaxy.domain.system

import java.util.regex.Pattern

data class SystemName(val value: String) {

    init {
        require(pattern.matcher(value).matches()) { "system.invalidName" }
    }

    override fun toString() = value

    companion object {
        private val pattern = Pattern.compile("([A-Z]{3})-([0-9]{3})")!!

        fun random(amount: Int): Set<SystemName> {
            require(amount > 0)
            require(amount <= 10_000_000)

            val systemNames = mutableSetOf<SystemName>()
            while (systemNames.size < amount) {
                val letters = getRandomLetters()
                val numbers = getRandomNumbers()
                systemNames.add(SystemName("$letters-$numbers"))
            }
            return systemNames
        }

        private fun getRandomLetters(): String {
            val allowedChars = ('A'..'Z')
            return (1..3)
                .map { allowedChars.random() }
                .joinToString("")
        }

        private fun getRandomNumbers(): String {
            val allowedChars = ('0'..'9')
            return (1..3)
                .map { allowedChars.random() }
                .joinToString("")
        }
    }
}
