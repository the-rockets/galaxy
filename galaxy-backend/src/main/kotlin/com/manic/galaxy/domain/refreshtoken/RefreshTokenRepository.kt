package com.manic.galaxy.domain.refreshtoken

import java.util.*

interface RefreshTokenRepository {
    suspend fun save(refreshToken: RefreshToken): RefreshToken
    suspend fun get(refreshTokenId: UUID): RefreshToken
    suspend fun delete(refreshToken: RefreshToken)
}
