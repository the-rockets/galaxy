package com.manic.galaxy.domain.goods

import com.manic.galaxy.domain.shared.Slice
import java.util.*

interface GoodsRepository {
    suspend fun save(goods: List<Goods>): List<Goods>
    suspend fun list(spaceStationId: UUID, slice: Slice): List<Goods>
    suspend fun get(spaceStationId: UUID, itemId: UUID): Goods
    suspend fun listByGalaxy(galaxyId: UUID, slice: Slice): List<Goods>
    suspend fun listByItem(itemId: UUID, slice: Slice): List<Goods>
}
