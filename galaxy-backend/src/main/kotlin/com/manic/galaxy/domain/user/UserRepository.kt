package com.manic.galaxy.domain.user

import java.util.*

interface UserRepository {
    suspend fun save(user: User): User
    suspend fun get(email: Email): User
    suspend fun get(id: UUID): User
    suspend fun requireUnique(email: Email)
}
