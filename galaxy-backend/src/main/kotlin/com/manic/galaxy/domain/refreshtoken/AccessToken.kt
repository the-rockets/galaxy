package com.manic.galaxy.domain.refreshtoken

import com.manic.galaxy.domain.shared.GalaxyTime
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.*

data class AccessToken(val userId: UUID, val expiresAt: Instant = GalaxyTime.now().plus(15, ChronoUnit.MINUTES))
