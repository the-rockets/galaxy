package com.manic.galaxy.domain.shared

data class Slice(val limit: Int, val offset: Int) {
    constructor() : this(100, 0)
    constructor(limit: Int?, offset: Int?) : this(limit?.coerceAtMost(100) ?: 100, offset?.coerceAtLeast(0) ?: 0)
}
