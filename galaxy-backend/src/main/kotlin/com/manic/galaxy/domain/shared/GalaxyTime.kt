package com.manic.galaxy.domain.shared

import java.time.Instant
import java.time.temporal.ChronoUnit

object GalaxyTime {
    private var now: Instant? = null

    fun now(): Instant {
        return now ?: Instant.now().truncatedTo(ChronoUnit.SECONDS)
    }

    fun setTime(instant: Instant) {
        now = instant
    }
}
