package com.manic.galaxy.domain.system

import java.util.*

class Graph(vertices: Set<UUID>, private val maxEdges: Int) {
    private val connectedVerticesByVertex = mutableMapOf<UUID, MutableList<UUID>>()
    private val vertices = vertices.toMutableSet()

    fun addEdge(a: UUID, b: UUID) {
        val edgesOfA = connectedVerticesByVertex.getOrPut(a, ::mutableListOf)
        val edgesOfB = connectedVerticesByVertex.getOrPut(b, ::mutableListOf)

        edgesOfA.add(b)
        if (edgesOfA.size == maxEdges) {
            vertices.remove(a)
        }
        edgesOfB.add(a)
        if (edgesOfB.size == maxEdges) {
            vertices.remove(b)
        }
    }

    fun getConnectedVertices(id: UUID): List<UUID> {
        return connectedVerticesByVertex.getOrPut(id, ::mutableListOf)
    }

    fun randomConnectedVertex(): UUID? {
        val connectedVertices = connectedVerticesByVertex.filterValues { it.isNotEmpty() && it.size < maxEdges }
        return when {
            connectedVertices.isNotEmpty() -> connectedVertices.keys.random()
            else -> null
        }
    }

    fun randomVertex(): UUID {
        return vertices.random()
    }
}
