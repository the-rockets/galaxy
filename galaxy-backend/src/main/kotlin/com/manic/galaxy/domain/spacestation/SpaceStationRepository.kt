package com.manic.galaxy.domain.spacestation

import com.manic.galaxy.domain.shared.Slice
import java.util.*

interface SpaceStationRepository {
    suspend fun save(spaceStation: SpaceStation): SpaceStation
    suspend fun list(systemId: UUID): List<SpaceStation>
    suspend fun list(galaxyId: UUID, slice: Slice): List<SpaceStation>
    suspend fun get(spaceStationId: UUID): SpaceStation
}
