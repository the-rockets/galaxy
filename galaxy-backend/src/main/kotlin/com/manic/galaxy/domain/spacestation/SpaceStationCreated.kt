package com.manic.galaxy.domain.spacestation

import java.util.*

data class SpaceStationCreated(
    val galaxyId: UUID,
    val systemId: UUID,
    val name: String,
    val id: UUID
)
