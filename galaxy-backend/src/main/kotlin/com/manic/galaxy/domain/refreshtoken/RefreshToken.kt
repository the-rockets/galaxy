package com.manic.galaxy.domain.refreshtoken

import com.manic.galaxy.domain.shared.Aggregate
import com.manic.galaxy.domain.shared.GalaxyTime
import com.manic.galaxy.domain.shared.NoArgConstructor
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.*

@NoArgConstructor
data class RefreshToken(
    val userId: UUID,
    val expiresAt: Instant = GalaxyTime.now().plus(1, ChronoUnit.DAYS),
    override val id: UUID,
): Aggregate() {

    fun refresh(): RefreshToken {
        return copy(expiresAt = GalaxyTime.now().plus(1, ChronoUnit.DAYS))
    }

    fun access(): AccessToken = AccessToken(userId)
}
