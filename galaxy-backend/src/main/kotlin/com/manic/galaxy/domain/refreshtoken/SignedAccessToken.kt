package com.manic.galaxy.domain.refreshtoken

data class SignedAccessToken(val value: String) {

    init {
        require(value.isNotEmpty()) { "signedAccessToken.invalid" }
    }
}
