package com.manic.galaxy.domain.user

interface PasswordEncryptor {
    fun encrypt(rawPassword: String): Password
    fun requireMatch(password: Password, rawPassword: String)
}
