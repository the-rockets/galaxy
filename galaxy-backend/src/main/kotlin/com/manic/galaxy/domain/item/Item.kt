package com.manic.galaxy.domain.item

import com.manic.galaxy.domain.shared.Aggregate
import java.util.*

data class Item(
    val galaxyId: UUID,
    val name: String,
    val type: ItemType,
    val volume: Long,
    val price: Long,
    override val id: UUID
) : Aggregate() {

    /**
     * Returns the price multiplied with the amount
     */
    fun calculateBasePrice(amount: Long): Long {
        return price * amount
    }

    fun calculateVolume(amount: Long): Long {
        return volume * amount
    }

    companion object {
        fun create(galaxyId: UUID): Set<Item> {
            return setOf(
                create(galaxyId, "Water", ItemType.FOOD, 10, 100),
                create(galaxyId, "Spices", ItemType.FOOD, 10, 200),
                create(galaxyId, "Herbs", ItemType.FOOD, 10, 200),
                create(galaxyId, "Ingredients", ItemType.FOOD, 10, 200),
                create(galaxyId, "Alcohol", ItemType.FOOD, 10, 500),
                create(galaxyId, "Meal", ItemType.FOOD, 10, 500),
            )
        }

        private fun create(galaxyId: UUID, name: String, type: ItemType, volume: Long, price: Long): Item {
            require(volume > 0) { "item.negativeVolume" }
            require(name.isNotBlank()) { "item.nameRequired" }
            return Item(galaxyId, name, type, volume, price, UUID.randomUUID())
        }
    }
}
