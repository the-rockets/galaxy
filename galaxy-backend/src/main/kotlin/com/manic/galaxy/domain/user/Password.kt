package com.manic.galaxy.domain.user

data class Password(val value: String) {

    init {
        require(value.isNotEmpty()) { "password.invalid" }
    }
    
}
