package com.manic.galaxy.domain.systemroute

import com.manic.galaxy.domain.shared.Aggregate
import java.time.Duration
import java.util.*

data class SystemRoute(
    val startId: UUID,
    val destinationId: UUID,
    val duration: Duration,
    override val id: UUID
) : Aggregate()
