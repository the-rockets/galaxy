package com.manic.galaxy.domain.refreshtoken

interface AccessTokenSigner {
    fun sign(accessToken: AccessToken): SignedAccessToken
    fun requireValid(signedAccessToken: SignedAccessToken)
}
