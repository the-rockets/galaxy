package com.manic.galaxy.domain.goods

import com.manic.galaxy.domain.shared.Aggregate
import java.util.*
import kotlin.math.roundToLong

data class Goods(
    val galaxyId: UUID,
    val spaceStationId: UUID,
    val itemId: UUID,
    val priceMultiplier: Double,
    override val id: UUID
) : Aggregate() {

    fun calculatePrice(price: Long): Long {
        return (price * priceMultiplier).roundToLong()
    }

    companion object {
        fun createFood(
            galaxyId: UUID,
            spaceStationId: UUID,
            itemIds: Set<UUID>,
            priceMultiplier: Double
        ): List<Goods> {
            return itemIds.map { itemId ->
                Goods(
                    galaxyId,
                    spaceStationId,
                    itemId,
                    priceMultiplier,
                    UUID.randomUUID()
                )
            }
        }
    }
}
