package com.manic.galaxy.domain.system

import com.manic.galaxy.domain.shared.Aggregate
import com.manic.galaxy.domain.shared.Probability
import java.util.*

data class System(
    val galaxyId: UUID,
    val name: SystemName,
    val inhabited: Boolean,
    val neighborSystemIds: List<UUID>,
    override val id: UUID,
): Aggregate() {
    fun requireInhabited() {
        require(inhabited) { "system.requireInhabited" }
    }

    companion object {


        fun create(galaxyId: UUID, names: Set<SystemName>): Set<System> {
            val inhabitedProbability = Probability(0.2)

            val systems = names
                .map { System(galaxyId, it, inhabitedProbability.next(), emptyList(), UUID.randomUUID()) }
                .toSet()

            val vertices = systems.map { it.id }.toSet()
            val graph = Graph(vertices, 4)

            for (system in systems) {
                val id = system.id
                val neighborCount = (Math.random() * 4.0 + 1).toInt()
                val edges = graph.getConnectedVertices(id)

                if (edges.isEmpty()) {
                    graph.randomConnectedVertex()?.let { graph.addEdge(id, it) }
                }

                while (edges.size < neighborCount) {
                    val potentialNeighborId = graph.randomVertex()
                    if (potentialNeighborId == id) continue
                    graph.addEdge(id, potentialNeighborId)
                }
            }

            return systems
                .map { it.copy(neighborSystemIds = graph.getConnectedVertices(it.id)) }
                .toSet()
                .onEach { it.addEvent(SystemCreated.of(it)) }
        }
    }
}

