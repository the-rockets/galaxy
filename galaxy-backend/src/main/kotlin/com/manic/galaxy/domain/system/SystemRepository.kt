package com.manic.galaxy.domain.system

import com.manic.galaxy.domain.shared.Slice
import java.util.*

interface SystemRepository {
    suspend fun requireUnique(galaxyId: UUID, names: Set<SystemName>)
    suspend fun save(systems: Set<System>): Set<System>
    suspend fun list(galaxyId: UUID, slice: Slice): List<System>
    suspend fun list(slice: Slice, ids: List<UUID>?): List<System>
    suspend fun randomInhabited(galaxyId: UUID): System
    suspend fun get(id: UUID): System
}
