package com.manic.galaxy.domain.galaxy

import java.util.*

data class GalaxyCreated(
    val galaxyId: UUID,
    val name: String,
    val id: UUID = UUID.randomUUID()
)
