package com.manic.galaxy.domain.systemroute

import com.manic.galaxy.domain.system.System
import java.time.Duration
import java.util.*
import kotlin.collections.ArrayDeque

class SystemRouter {
    fun routeGalaxy(systems: Collection<System>): Set<SystemRoute> {
        val graph = systems.groupBy { it.id }.mapValues { it.value.flatMap { it.neighborSystemIds } }
        return graph.keys.map { routeSystem(it, graph) }.flatten().toSet()
    }

    private fun routeSystem(systemId: UUID, neighborsBySystemId: Map<UUID, List<UUID>>): List<SystemRoute> {
        val linkedList = breadthFirstSearch(neighborsBySystemId, systemId)

        return linkedList.keys.map { link ->
            var jumps: Long = 0
            var prevCameFrom: UUID? = null
            do {
                prevCameFrom = linkedList[prevCameFrom ?: link]
                if (prevCameFrom != null) {
                    jumps++
                }
            } while (prevCameFrom != null)

            val duration = Duration.ofMinutes(jumps)
            SystemRoute(
                systemId,
                link,
                duration,
                UUID.randomUUID()
            )
        }
    }

    private fun breadthFirstSearch(ids: Map<UUID, List<UUID>>, root: UUID): MutableMap<UUID, UUID?> {
        val discovered = mutableSetOf<UUID>()
        val queue = ArrayDeque<UUID>()

        val systemIdByFoundBySystemId = mutableMapOf<UUID, UUID?>()

        systemIdByFoundBySystemId[root] = null
        discovered.add(root)
        queue.add(root)

        while (queue.isNotEmpty()) {
            val system = queue.removeFirst()

            for (neighbor in ids[system]!!) {
                if (!discovered.contains(neighbor)) {
                    systemIdByFoundBySystemId[neighbor] = system
                    discovered.add(neighbor)
                    queue.add(neighbor)
                }
            }
        }

        return systemIdByFoundBySystemId
    }
}
