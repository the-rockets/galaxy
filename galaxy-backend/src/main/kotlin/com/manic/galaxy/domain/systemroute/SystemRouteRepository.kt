package com.manic.galaxy.domain.systemroute

import com.manic.galaxy.domain.shared.Slice
import java.util.*

interface SystemRouteRepository {
    suspend fun save(systemRoutes: Set<SystemRoute>): Set<SystemRoute>
    suspend fun list(startId: UUID, slice: Slice): List<SystemRoute>
    suspend fun get(startId: UUID, destinationId: UUID): SystemRoute
}
