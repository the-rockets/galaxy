package com.manic.galaxy.domain.cargo

import com.manic.galaxy.domain.shared.Aggregate
import java.util.*

data class Cargo(
    val pilotId: UUID,
    val itemId: UUID,
    val volumePerUnit: Long,
    val amount: Long,
    override val id: UUID
) : Aggregate() {

    val volume = amount * volumePerUnit

    fun add(amount: Long) = copy(
        amount = this.amount + amount
    )

    fun remove(amount: Long): Cargo {
        val result = this.volume - volume
        require(result >= 0) { "cargo.notEnoughItems" }

        return copy(
            amount = this.amount - amount
        )
    }

    fun isEmpty() = volume == 0L

    companion object {
        fun create(pilotId: UUID, itemId: UUID, volumePerUnit: Long, amount: Long): Cargo {
            return Cargo(
                pilotId,
                itemId,
                volumePerUnit,
                amount,
                UUID.randomUUID()
            )
        }
    }
}
