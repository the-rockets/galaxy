package com.manic.galaxy.domain.system

import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import kotlin.system.measureTimeMillis
import kotlin.test.assertEquals

class SystemNameTest {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(this::class.java)
    }

    @Test
    fun `random name should produce unique names`() {
        val systems = 1_000_000

        var set: Set<SystemName>
        val ms = measureTimeMillis {
            set = SystemName.random(systems)
        }
        logger.info("Generating $systems system names took ${ms}ms")
        assertEquals(systems, set.size, "System names are not unique.")
    }
}
