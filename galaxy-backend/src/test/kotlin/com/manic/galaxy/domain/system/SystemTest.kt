package com.manic.galaxy.domain.system

import com.manic.galaxy.domain.systemroute.SystemRouter
import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.time.Duration
import java.util.*
import kotlin.collections.ArrayDeque
import kotlin.system.measureTimeMillis
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class SystemTest {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(this::class.java)
    }

    @Test
    fun `system route should contain only self with zero travel time duration`()     {
        val systemCount = 100

        val galaxyId = UUID.randomUUID()
        val names = SystemName.random(systemCount)
        val systems = System.create(galaxyId, names)

        val current = systems.random().id

        val service = SystemRouter()
        val systemRoutes = service.routeGalaxy(systems)
        val filtered = systemRoutes.filter { it.startId == current }.filter { it.duration == Duration.ZERO }
        assertEquals(1, filtered.size)
        assertEquals(current, filtered.first().destinationId)
    }

    @Test
    fun `creating systems should create a fully linked graph`() {
        val systemCount = 1_000

        val galaxyId = UUID.randomUUID()
        val names = SystemName.random(systemCount)
        val systems: Set<System>
        val ms = measureTimeMillis {
            systems = System.create(galaxyId, names)
        }

        logger.info("Generating $systemCount systems took ${ms}ms")
        val systemsById = systems.associateBy { it.id }.mapValues { it.value.neighborSystemIds }


        val randomSystem = systemsById.keys.random()
        val foundNeighborIds: Set<UUID> = breadthFirstSearch(systemsById, randomSystem)

        assertTrue(systems.all { it.neighborSystemIds.size <= 4 })

        assertEquals(
            foundNeighborIds.size,
            names.size,
            "Systems are not fully linked."
        )
    }

    private fun breadthFirstSearch(ids: Map<UUID, List<UUID>>, root: UUID): Set<UUID> {
        val discovered = mutableSetOf<UUID>()
        val queue = ArrayDeque<UUID>()

        discovered.add(root)
        queue.add(root)

        while (queue.isNotEmpty()) {
            val system = queue.removeFirst()

            for (neighbor in ids[system]!!) {
                if (!discovered.contains(neighbor)) {
                    discovered.add(neighbor)
                    queue.add(neighbor)
                }
            }
        }

        return discovered
    }
}
