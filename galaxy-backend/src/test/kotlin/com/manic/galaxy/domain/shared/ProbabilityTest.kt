package com.manic.galaxy.domain.shared

import org.junit.Assert.assertEquals
import org.junit.Test


class ProbabilityTest {

    @Test
    fun `getting next probability should result in correct rate`() {
        val value = 0.1
        val probability = Probability(value)

        val total = 1_000_000
        val successCounter = (0..total).asSequence().map { probability.next() }.count { it }

        assertEquals(value, successCounter / total.toDouble(), 0.01)
    }
}
