package com.manic.galaxy

import com.manic.galaxy.infrastructure.event.TestEventService
import com.manic.galaxy.infrastructure.ktor.Environment
import com.manic.galaxy.infrastructure.ktor.createEnvironment
import org.testcontainers.containers.MongoDBContainer
import org.testcontainers.utility.DockerImageName

object Galaxy {
    val environment: Environment

    init {
        val mongoDBContainer = MongoDBContainer(DockerImageName.parse("mongo:4.2.5"))
        mongoDBContainer.start()

        environment = createEnvironment(
            mongoDBContainer.getReplicaSetUrl("galaxy"),
            "galaxy",
            TestEventService()
        )
    }
}
