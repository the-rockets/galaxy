package com.manic.galaxy.application

import com.github.codefabrikgmbh.scenarios.given
import com.manic.galaxy.IntegrationTest
import com.manic.galaxy.infrastructure.scenarios.User
import com.manic.galaxy.infrastructure.scenarios.`then the user should see a galaxy list with n items`
import com.manic.galaxy.infrastructure.scenarios.`when the system creates a galaxy`
import kotlin.test.Test

class GalaxyServiceTest: IntegrationTest() {

    @Test
    fun `creating a galaxy should work`() {
        given(::User) {
            `when the system creates a galaxy`()
            `then the user should see a galaxy list with n items`(n = 1)
        }
    }
}

