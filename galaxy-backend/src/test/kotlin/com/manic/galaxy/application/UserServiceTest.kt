package com.manic.galaxy.application

import com.github.codefabrikgmbh.scenarios.Scenario
import com.github.codefabrikgmbh.scenarios.given
import com.manic.galaxy.IntegrationTest
import com.manic.galaxy.domain.user.Email
import com.manic.galaxy.infrastructure.scenarios.*
import org.junit.Test

class UserServiceTest : IntegrationTest() {

    @Test
    fun `signing in with unknown email address should fail`() {
        given(::Scenario, expected = IllegalArgumentException::class) {
            `when i sign in as`(Email("admin@galaxy.com"), "password")
        }
    }

    @Test
    fun `signing in as a user should work`() {
        given({ User(Email("admin@galaxy.com"), "password") }) {
            `when i sign in as`(Email("admin@galaxy.com"), "password")
        }
    }

    @Test
    fun `signing in with an invalid password should fail`() {
        given({ User(Email("admin@galaxy.com"), "password") }, expected = IllegalArgumentException::class) {
            `when i sign in as`(Email("admin@galaxy.com"), "invalid")
        }
    }

    @Test
    fun `selecting a pilot should work`() {
        given(::pilot) {
            `when the user selects the pilot`()
            `then the user should have a selected pilot`(pilot.id)
        }
    }
}

