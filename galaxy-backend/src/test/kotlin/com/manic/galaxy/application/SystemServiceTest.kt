package com.manic.galaxy.application

import com.github.codefabrikgmbh.scenarios.given
import com.manic.galaxy.IntegrationTest
import com.manic.galaxy.infrastructure.scenarios.`cepheus galaxy`
import com.manic.galaxy.infrastructure.scenarios.`then the user should see a system list with n items`
import kotlin.test.Test

class SystemServiceTest : IntegrationTest() {

    @Test
    fun `creating a galaxy should create systems`() {
        given(::`cepheus galaxy`) {
            `then the user should see a system list with n items`(n = 100)
        }
    }
}

