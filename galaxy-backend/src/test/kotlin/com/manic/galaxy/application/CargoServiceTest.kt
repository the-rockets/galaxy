package com.manic.galaxy.application

import com.github.codefabrikgmbh.scenarios.given
import com.manic.galaxy.IntegrationTest
import com.manic.galaxy.infrastructure.scenarios.*
import org.junit.Test

class CargoServiceTest : IntegrationTest() {

    @Test
    fun `buying water should reduce your credits`() {
        given(::`docked pilot`) {
            `when the pilot buys n water`(n = 1)
            `then the pilot should have less than n credits`(n = pilot.credits)
        }
    }

    @Test
    fun `buying water should create cargo`() {
        given(::`docked pilot`) {
            `when the pilot buys n water`(n = 1)
            `then the pilot should have n cargo`(n = 1)
        }
    }

    @Test
    fun `buying water should increase the pilot storage weight`() {
        given(::`docked pilot`) {
            `when the pilot buys n water`(n = 1)
            `then the pilot should have n storage volume`(n = water.volume)
        }
    }

    @Test
    fun `buying water while not being docked should fail`() {
        given(::pilot, expected = IllegalArgumentException::class) {
            `when the pilot buys n water`(n = 1)
        }
    }

    @Test
    fun `selling water should increase your credits`() {
        given(::`docked pilot`) {
            `when the pilot buys n water`(n = 1)
            `when the pilot sells n water`(n = 1)
            `then the pilot should have n credits`(n = pilot.credits)
        }
    }

    @Test
    fun `selling water should remove cargo if empty`() {
        given(::`docked pilot`) {
            `when the pilot buys n water`(n = 1)
            `when the pilot sells n water`(n = 1)
            `then the pilot should have n cargo`(n = 0)
        }
    }

    @Test
    fun `selling water should reduce the pilot storage weight`() {
        given(::`docked pilot`) {
            `when the pilot buys n water`(n = 1)
            `when the pilot sells n water`(n = 1)
            `then the pilot should have n storage volume`(n = 0)
        }
    }
}


