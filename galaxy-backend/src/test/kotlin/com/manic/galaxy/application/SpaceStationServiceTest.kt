package com.manic.galaxy.application

import com.github.codefabrikgmbh.scenarios.given
import com.manic.galaxy.IntegrationTest
import com.manic.galaxy.infrastructure.scenarios.`cepheus galaxy`
import com.manic.galaxy.infrastructure.scenarios.`then the user should see a randoms inhabited space stations goods list with n items`
import com.manic.galaxy.infrastructure.scenarios.`then the user should see a randoms inhabited system space station list with n items`
import org.junit.Test

class SpaceStationServiceTest : IntegrationTest() {

    @Test
    fun `creating a galaxy should create systems with space stations`() {
        given(::`cepheus galaxy`) {
            `then the user should see a randoms inhabited system space station list with n items`(n = 1)
        }
    }

    @Test
    fun `creating a galaxy should create systems with space stations with goods`() {
        given(::`cepheus galaxy`) {
            `then the user should see a randoms inhabited space stations goods list with n items`(n = 6)
        }
    }
}

