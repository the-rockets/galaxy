package com.manic.galaxy.application

import com.github.codefabrikgmbh.scenarios.given
import com.manic.galaxy.IntegrationTest
import com.manic.galaxy.infrastructure.scenarios.*
import kotlin.test.Test

class PilotServiceTest : IntegrationTest() {

    @Test
    fun `creating a pilot should work`() {
        given(::`cepheus galaxy`) {
            `when the user creates a pilot`(name = "Manic")
            `then the user should see a pilot list with n items`(n = 1)
        }
    }

    @Test
    fun `creating multiple pilots for the same galaxy should fail`() {
        given(::`cepheus galaxy`, expected = IllegalArgumentException::class) {
            `when the user creates a pilot`(name = "Manic")
            `when the user creates a pilot`(name = "Manic")
        }
    }

    @Test
    fun `flying to a system should create a flying destination`() {
        given(::pilot) {
            `when the pilot flies to a random system`()
            `then the pilot should have a flying destination`()
        }
    }

    @Test
    fun `reaching a destination should update the pilot`() {
        given(::`flying pilot`) {
            `when the flying pilot arrives at the destination`()
            `then the pilot should be at the destination`()
        }
    }

    @Test
    fun `docking at a space station should work`() {
        given(::pilot) {
            `when the pilot docks at random space station in the pilot's system`()
            `then the pilot should be docked at a space station`()
        }
    }

    @Test
    fun `flying to a system while docked should undock the pilot`() {
        given(::`docked pilot`) {
            `when the pilot flies to a random system`()
            `then the pilot should be undocked`()
        }
    }
}




