package com.manic.galaxy

import com.manic.galaxy.infrastructure.mongodb.toList
import com.manic.galaxy.infrastructure.mongodb.toUnit
import com.mongodb.client.model.Filters
import kotlinx.coroutines.runBlocking
import org.junit.Before

abstract class IntegrationTest {
    @Before
    fun `clean up`() = runBlocking {
        Galaxy.environment.database
            .listCollectionNames()
            .toList()
            .map(Galaxy.environment.database::getCollection)
            .forEach {
                it.deleteMany(Filters.exists("_id")).toUnit()
            }
    }
}
