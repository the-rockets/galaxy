package com.manic.galaxy.infrastructure.scenarios

import com.github.codefabrikgmbh.scenarios.Scenario
import com.manic.galaxy.Galaxy
import com.manic.galaxy.domain.shared.GalaxyTime
import com.manic.galaxy.domain.shared.Slice
import com.manic.galaxy.domain.user.Email
import com.manic.galaxy.infrastructure.mongodb.MongoPilotRepository
import kotlinx.coroutines.runBlocking

fun `when i sign in as`(email: Email, password: String) = runBlocking {
    Galaxy.environment.userService.signIn(email, password)
}

fun Scenario.`when the system creates a galaxy`() = runBlocking {
    Galaxy.environment.galaxyService.createGalaxy("Cepheus")
}

fun `cepheus galaxy`.`when the user creates a pilot`(name: String) = runBlocking {
    Galaxy.environment.pilotService.createPilot(user.id, cepheus.id, name)
}

fun pilot.`when the pilot flies to a random system`() = runBlocking {
    val system = Galaxy.environment.systemService.listSystems(cepheus.id, Slice()).first()
    Galaxy.environment.pilotService.flyTo(user.id, pilot.id, system.id)
    Unit
}

fun pilot.`when the user selects the pilot`() = runBlocking {
    Galaxy.environment.userService.selectPilot(user.id, pilot.id)
}

fun `flying pilot`.`when the flying pilot arrives at the destination`() = runBlocking {
    GalaxyTime.setTime(flyingPilot.flyingDestination!!.arrivalAt)
    (Galaxy.environment.pilotRepository as MongoPilotRepository).findArrived().forEach {
        Galaxy.environment.pilotService.reachDestinations(it.id)
    }
}

fun pilot.`when the pilot docks at random space station in the pilot's system`() = runBlocking {
    val spaceStation = Galaxy.environment.spaceStationService.listSpaceStations(pilot.systemId).first()
    Galaxy.environment.pilotService.dockAt(pilot.userId, pilot.id, spaceStation.id)
}

fun pilot.`when the pilot buys n water`(n: Long) = runBlocking {
    Galaxy.environment.tradingService.buyItem(pilot.userId, pilot.id, water.id, n)
}

fun pilot.`when the pilot sells n water`(n: Long) = runBlocking {
    Galaxy.environment.tradingService.sellItem(pilot.userId, pilot.id, water.id, n)
}
