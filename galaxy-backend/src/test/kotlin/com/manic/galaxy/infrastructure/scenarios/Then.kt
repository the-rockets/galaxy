package com.manic.galaxy.infrastructure.scenarios

import com.manic.galaxy.Galaxy
import com.manic.galaxy.domain.shared.Slice
import kotlinx.coroutines.runBlocking
import java.util.*
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull
import kotlin.test.assertTrue

fun User.`then the user should see a galaxy list with n items`(n: Int) = runBlocking {
    val galaxies = Galaxy.environment.galaxyService.listGalaxies(Slice())
    assertEquals(n, galaxies.size, "Expected $n galaxies.")
}

fun `cepheus galaxy`.`then the user should see a system list with n items`(n: Int) = runBlocking {
    val systems = Galaxy.environment.systemService.listSystems(cepheus.id, Slice(n, 0))
    assertEquals(n, systems.size)
}

fun `cepheus galaxy`.`then the user should see a pilot list with n items`(n: Int) = runBlocking {
    val slice = Galaxy.environment.pilotService.listPilots(user.id, Slice())
    assertEquals(n, slice.size)
}

fun pilot.`then the pilot should have a flying destination`() = runBlocking {
    val pilot = Galaxy.environment.pilotService.get(user.id, pilot.id)
    assertNotNull(pilot.flyingDestination)
}

fun pilot.`then the user should have a selected pilot`(pilotId: UUID) = runBlocking {
    val user = Galaxy.environment.userService.getUser(user.id)
    assertEquals(user.selectedPilotId, pilotId)
}

fun `flying pilot`.`then the pilot should be at the destination`() = runBlocking {
    val pilot = Galaxy.environment.pilotService.get(user.id, flyingPilot.id)
    assertEquals(destination.id, pilot.systemId)
}

fun `cepheus galaxy`.`then the user should see a randoms inhabited system space station list with n items`(n: Int) = runBlocking {
    val list = Galaxy.environment.spaceStationService.listSpaceStations(systems.filter { it.inhabited }.random().id)
    assertEquals(n, list.size)
}

fun `cepheus galaxy`.`then the user should see a randoms inhabited space stations goods list with n items`(n: Int) = runBlocking {
    val spaceStation = Galaxy.environment.spaceStationService.listSpaceStations(systems.filter { it.inhabited }.random().id).first()
    val goods = Galaxy.environment.goodsService.listGoods(spaceStation.id, Slice())
    assertEquals(n, goods.size)
}

fun pilot.`then the pilot should be docked at a space station`() = runBlocking {
    val pilot = Galaxy.environment.pilotService.get(pilot.userId, pilot.id)
    assertNotNull(pilot.spaceStationId)
}

fun `docked pilot`.`then the pilot should be undocked`() = runBlocking {
    val pilot = Galaxy.environment.pilotService.get(pilot.userId, pilot.id)
    assertNull(pilot.spaceStationId)
}

fun pilot.`then the pilot should have n credits`(n: Long) = runBlocking {
    val pilot = Galaxy.environment.pilotService.get(pilot.userId, pilot.id)
    assertEquals(n, pilot.credits)
}

fun pilot.`then the pilot should have less than n credits`(n: Long) = runBlocking {
    val pilot = Galaxy.environment.pilotService.get(pilot.userId, pilot.id)
    assertTrue(pilot.credits < n)
}

fun pilot.`then the pilot should have n cargo`(n: Int) = runBlocking {
    val list = Galaxy.environment.cargoService.listCargo(pilot.userId, pilot.id, Slice())
    assertEquals(n, list.size)
}

fun pilot.`then the pilot should have n storage volume`(n: Long) = runBlocking {
    val pilot = Galaxy.environment.pilotService.get(pilot.userId, pilot.id)
    assertEquals(n, pilot.storageVolume)
}
