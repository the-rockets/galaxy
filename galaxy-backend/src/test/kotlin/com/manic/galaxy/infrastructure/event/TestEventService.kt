package com.manic.galaxy.infrastructure.event

import kotlin.reflect.KClass
import kotlin.reflect.cast

@Suppress("UNCHECKED_CAST")
class TestEventService : EventService {
    private val subscribersByEvent = mutableMapOf<KClass<out Any>, List<suspend (Any) -> Unit>>()

    override suspend fun publish(events: List<Any>) {
        events.forEach { event ->
            val kClass = event::class
            subscribersByEvent[kClass]
                ?.forEach { subscriber -> subscriber(kClass.cast(event)) }
        }
    }

    override fun <T : Any> subscribe(event: KClass<T>, callback: suspend (T) -> Unit) {
        val subscribers = subscribersByEvent.getOrDefault(event, emptyList())
        subscribersByEvent[event] = (subscribers + callback) as List<suspend (Any) -> Unit>
    }
}
