package com.manic.galaxy.infrastructure.scenarios

import com.github.codefabrikgmbh.scenarios.Scenario
import com.manic.galaxy.Galaxy
import com.manic.galaxy.domain.pilot.Pilot
import com.manic.galaxy.domain.shared.Slice
import com.manic.galaxy.domain.system.System
import com.manic.galaxy.domain.user.Email
import com.manic.galaxy.domain.user.User
import kotlinx.coroutines.runBlocking

open class User(
    val userEmail: Email = Email("admin@galaxy.com"),
    val userPassword: String = "password"
) : Scenario() {
    val user: User = runBlocking {
        Galaxy.environment.userService.createUser(userEmail, userPassword)
    }

}

open class `cepheus galaxy`() : com.manic.galaxy.infrastructure.scenarios.User() {
    val cepheus: com.manic.galaxy.domain.galaxy.Galaxy = runBlocking {
        Galaxy.environment.galaxyService.createGalaxy("Cepheus")
    }

    val systems = runBlocking {
        Galaxy.environment.systemService.listSystems(cepheus.id, Slice(0,0))
    }

    val water = runBlocking {
        Galaxy.environment.itemService.listItems(cepheus.id, Slice()).first { it.name == "Water" }
    }
}

open class `pilot` : `cepheus galaxy`() {
    open val pilot: Pilot = runBlocking {
        Galaxy.environment.pilotService.createPilot(user.id, cepheus.id, "Pilot")
    }
}

class `docked pilot` : pilot() {
    override val pilot: Pilot = runBlocking {
        val spaceStation = Galaxy.environment.spaceStationService.listSpaceStations(super.pilot.systemId).first()
        Galaxy.environment.pilotService.dockAt(user.id, super.pilot.id, spaceStation.id)
    }
}

class `flying pilot` : `cepheus galaxy`() {
    val flyingPilot: Pilot
    val destination: System

    init {
        val pilot = runBlocking { Galaxy.environment.pilotService.createPilot(user.id, cepheus.id, "Pilot") }
        destination = runBlocking {
            Galaxy.environment.systemService
                .listSystems(cepheus.id, Slice())
                .filter { it.id != pilot.systemId }
                .random()
        }
        flyingPilot = runBlocking { Galaxy.environment.pilotService.flyTo(user.id, pilot.id, destination.id) }
    }
}
