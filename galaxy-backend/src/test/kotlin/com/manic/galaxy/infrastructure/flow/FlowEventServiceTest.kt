package com.manic.galaxy.infrastructure.flow

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Test

class FlowEventServiceTest {

    data class Event(val name: String)

    @Test
    fun `subscribing should work`() = runBlocking {
        val service = FlowEventService(GlobalScope)
        var subscriptionCallCount = 0

        service.subscribe(Event::class) {
            subscriptionCallCount++
        }
        delay(100)
        service.publish(listOf(Event("test")))
        delay(100)

        assertEquals(1, subscriptionCallCount)
    }
}
