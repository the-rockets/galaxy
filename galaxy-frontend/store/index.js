export const state = () => ({
  pilots: {},
  cargo: {
    byId: {}
  },
  systems: {},
  systemRoutes: {
    byId: {},
    byStartId: {}
  },
  galaxies: {},
  spaceStations: {
    byId: {},
    bySystemId: {}
  },
  items: {
    byId: {}
  },
  goods: {
    byId: {},
    bySpaceStationId: {}
  },
  websockets: {
    events: null
  }
})

const normalize = (state, value, field) => {
  return value.reduce((prev, curr) => {
    prev[curr[field]] = curr
    return prev
  }, state)
}

const collect = (state, value, field, collectedField) => {
  return value.reduce((prev, curr) => {
    const array = (prev[curr[field]] || [])
    const currValue = curr[collectedField]
    if (!array.includes(currValue)) {
      array.push(currValue)
    }
    prev[curr[field]] = array
    return prev
  }, state)
}

export const mutations = {
  setPilots (state, value) {
    state.pilots = normalize({ ...state.pilots }, value, 'id')
  },
  setCargo (state, value) {
    state.cargo = {
      byId: normalize({ }, value, 'id')
    }
  },
  setSystems (state, value) {
    state.systems = normalize({ ...state.systems }, value, 'id')
  },
  setSystemRoutes (state, value) {
    state.systemRoutes = {
      byId: normalize({ ...state.systemRoutes.byId }, value, 'id'),
      byStartId: collect({ ...state.systemRoutes.byStartId }, value, 'startId', 'id')
    }
  },
  setGalaxies (state, value) {
    state.galaxies = normalize({ ...state.galaxies }, value, 'id')
  },
  setSpaceStations (state, value) {
    state.spaceStations = {
      byId: normalize({ ...state.spaceStations.byId }, value, 'id'),
      bySystemId: collect({ ...state.spaceStations.bySystemId }, value, 'systemId', 'id')
    }
  },
  setGoods (state, value) {
    state.goods = {
      byId: normalize({ ...state.goods.byId }, value, 'id'),
      bySpaceStationId: collect({ ...state.goods.bySpaceStationId }, value, 'spaceStationId', 'id')
    }
  },
  setItems (state, value) {
    state.items = {
      byId: normalize({ ...state.items.byId }, value, 'id')
    }
  },
  setEventWebSocket (state, value) {
    state.websockets.events = value
  }
}

export const getters = {
  selectedPilot: state => state.auth.user ? state.pilots[state.auth.user.selectedPilotId] : null,
  currentSpaceStations: (state, getters) => {
    const pilot = getters.selectedPilot
    if (!pilot) {
      return []
    }
    const spaceStationIds = state.spaceStations.bySystemId[pilot.systemId] || []
    return spaceStationIds.map(it => state.spaceStations.byId[it])
  },
  systemOverview: (state, getters) => {
    const spaceStations = getters.currentSpaceStations
    if (spaceStations.length === 0) {
      return []
    }
    const pilot = getters.selectedPilot
    const currentSystem = state.systems[pilot.systemId]
    const neighborSystems = currentSystem.neighborSystemIds.map(it => state.systems[it])
    return [
      ...spaceStations.map(it => ({ ...it, type: 'STATION' })),
      ...neighborSystems.map(it => ({ ...it, type: 'SYSTEM' }))
    ]
  },
  systemRouteList: (state, getters) => {
    const systems = state.systemRoutes.byStartId[getters.selectedPilot.systemId]
    if (!systems) {
      return []
    }
    return systems
      .map(it => state.systemRoutes.byId[it])
      .map(it => ({
        ...it,
        destination: state.systems[it.destinationId]
      }))
  }
}

export const actions = {
  fetchPilots ({ commit }) {
    return this.$axios.$get('/api/pilots')
      .then(it => commit('setPilots', it))
  },
  fetchSystems ({ commit, getters }) {
    return this.$axios.$get(`/api/galaxies/${getters.selectedPilot.galaxyId}/systems`)
      .then(it => commit('setSystems', it))
  },
  fetchSystemRoutes ({ commit, getters }) {
    return this.$axios.$get(`/api/systems/${getters.selectedPilot.systemId}/routes`)
      .then(it => commit('setSystemRoutes', it))
  },
  fetchGalaxies ({ commit }) {
    return this.$axios.$get('/api/galaxies')
      .then(it => commit('setGalaxies', it))
  },
  fetchSpaceStations ({ commit, getters }) {
    return this.$axios.$get(`/api/galaxies/${getters.selectedPilot.galaxyId}/space-stations`)
      .then(it => commit('setSpaceStations', it))
  },
  fetchGoods ({ commit }, spaceStationId) {
    return this.$axios.$get(`/api/space-stations/${spaceStationId}/goods`)
      .then(it => commit('setGoods', it))
  },
  fetchItems ({ commit, getters }) {
    return this.$axios.$get(`/api/galaxies/${getters.selectedPilot.galaxyId}/items`)
      .then(it => commit('setItems', it))
  },
  fetchCargo ({ commit, getters }) {
    return this.$axios.$get(`/api/pilots/${getters.selectedPilot.id}/cargo`)
      .then(it => commit('setCargo', it))
  },
  flyTo ({ commit, getters }, data) {
    return this.$axios.$post(`/api/pilots/${getters.selectedPilot.id}/fly-to`, data)
      .then(it => commit('setPilots', [it]))
  },
  dockAt ({ commit, getters }, data) {
    return this.$axios.$post(`/api/pilots/${getters.selectedPilot.id}/dock-at`, data)
      .then(it => commit('setPilots', [it]))
  },
  buyItem ({ commit, getters, dispatch }, data) {
    return this.$axios.$post(`/api/pilots/${getters.selectedPilot.id}/buy-item`, data)
      .then(it => commit('setPilots', [it]))
      .then(() => dispatch('fetchCargo'))
  },
  sellItem ({ commit, getters, dispatch }, data) {
    return this.$axios.$post(`/api/pilots/${getters.selectedPilot.id}/sell-item`, data)
      .then(it => commit('setPilots', [it]))
      .then(() => dispatch('fetchCargo'))
  },
  selectPilot (context, data) {
    return this.$axios.$post('/api/users/self/select-pilot', data)
      .then(it => this.$auth.setUser(it))
  },
  reachDestination ({ commit, dispatch, state }, data) {
    const pilot = state.pilots[data.pilotId]
    if (pilot === undefined) {
      return
    }
    const copy = { ...pilot }
    copy.flyingDestination = null
    copy.systemId = data.systemId
    commit('setPilots', [copy])
    dispatch('fetchSpaceStations')
  }
}
