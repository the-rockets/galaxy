import Vue from 'vue'
import { formatDistanceToNow, formatDuration } from 'date-fns'

Vue.filter('duration', (value) => {
  if (!value) {
    return ''
  }

  if (typeof value === 'string') {
    return formatDistanceToNow(new Date(value))
  }

  return formatDuration({
    minutes: value / 60,
    seconds: value % 60
  }, {
    format: ['minutes', 'seconds']
  })
})

Vue.filter('else', (value, defaultValue) => value || defaultValue)
