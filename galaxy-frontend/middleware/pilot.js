export default function ({ store, redirect }) {
  if (store.getters.selectedPilot == null) {
    return redirect('/pilots')
  }
}
